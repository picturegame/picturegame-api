# Round

The `Round` object contains information about a single round.

## Properties

### roundNumber
The unique round number for the round.
* Type: `integer`
* Example: `40000`

### title
The title of the round, including the [Round XXXXX] prefix.
* Type: `string`
* Example: `"[Round 40000] What lighthouse?"`

### postUrl
The url of the picture for the round.
* Type: `string`
* Example: `"https://i.imgur.com/SAsYPaJ.png"`

### thumbnailUrl
The url of the thumbnail for the round's picture, as provided by Reddit.
* Type: `string`
* Example: `"https://b.thumbs.redditmedia.com/gZvQq0O7CJkhooCTrsEwvz3rgfNJo29Z2vf8dgRQF7o.jpg"`

### id
The base-36 Reddit ID for the round's submission.
* Type: `string`
* Example: `"763umq"`

### hostName
The Reddit username for the host of the round.
* Type: `string`
* Example: `"Trehorna123"`

### postTime
The time that the round was submitted, as a Unix time stamp (seconds since midnight 1970-01-01 UTC)
* Type: `integer`
* Example: `1507889585`

### winningCommentId
The base-36 Reddit ID for the round's winning comment. Not defined if the round has not been won.
* Type: `string`
* Example: `"dob2bf8"`

### winnerName
The Reddit username for the winner of the round. Not defined if the round has not been won.
* Type: `string`
* Example: `"SuperFreakonomics"`

### winTime
The time that the winning comment for the round was submitted, as a Unix time stamp (seconds since midnight 1970-01-01 UTC). Not defined if the round has not been won.
* Type: `integer`
* Example: `1507889654`

### plusCorrectTime
The time that the +correct comment for the round was submitted, as a Unix time stamp (seconds since midnight 1970-01-01 UTC). Not defined if the round has not been won.
* Type: `integer`
* Example: `1507889793`

### postDelay
The amount of time it took to post the round, in seconds. Calculated as (`postTime` of this round) - (`plusCorrectTime` of the previous round).
* Type: `integer`
* Example: `180`

Not defined in the following cases, as we assume that the previous round was abandoned:
* The previous round, or its `plusCorrectTime`, are not defined
* The previous round's `plusCorrectTime` is after this round's `postTime`
* The previous round's `winnerName` is not the same as this round's `hostName`

### solveTime
The amount of time it took to solve the round, in seconds. Calculated as `winTime` - `postTime`. Not defined if the round has not been won.
* Type: `integer`
* Example: `3600`

### plusCorrectDelay
The amount of time it took to +correct the winning comment, in seconds. Calculated as `plusCorrectTime` - `winTime`. Not defined if the round has not been won.
* Type: `integer`
* Example: `45`

### postTimeOfDay
The time of day that the round was posted at. Represented as the number of seconds since midnight, UTC
* Type: `integer`
* Example: `43200`

### postDayOfWeek
The day of the week that the round was posted on, in UTC. 0 = Sunday, 6 = Saturday
* Type: `integer`
* Example: `3`

### winTimeOfDay
The time of day that the round was won at. Represented as the number of seconds since midnight, UTC
* Type: `integer`
* Example: `43200`

### winDayOfWeek
The day of the week that the round was won on, in UTC. 0 = Sunday, 6 = Saturday
* Type: `integer`
* Example: `3`

### abandoned
Whether or not the round was abandoned.
True if the round has not been solved and the next round has been posted,
or if the round was solved after the next round was posted.
* Type: `boolean`
* Example: `false`

### abandonedTime
The time that the round was abandoned, as a Unix time stamp (seconds since midnight 1970-01-01 UTC). Not defined if the round was not abandoned. Continues to be set if the round was subsequently solved.
Not set for rounds prior to the release of API 1.11.0 (approx August 2022). This may be backfilled as future work.
* Type: `integer`
* Example: `1507889793`
