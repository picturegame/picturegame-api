# Player

The `Player` object contains information about a player and the rounds they have won and hosted.

## Properties

### username
The Reddit username of the player.
* Type: `string`

### stats
An object containing stats for the player.
* Type: `object`

### stats.numRoundsWon
The number of rounds the player has won.
* Type: `integer`

### stats.numRoundsHosted
The number of rounds the player has hosted.
* Type: `integer`

### stats.avgSolveTime
The average number of seconds taken to solve rounds won by the player.
* Type: `number`

### stats.minSolveTime
The minimum number of seconds taken to solve any round won by the player.
* Type: `integer`

### stats.maxSolveTime
The maximum number of seconds taken to solve any round won by the player.
* Type: `integer`

### stats.avgPostDelay
The average number of seconds this player took to post a round after being +corrected.
* Type: `number`

### stats.minPostDelay
The minimum number of seconds this player took to post a round after being +corrected.
* Type: `integer`

### stats.maxPostDelay
The maximum number of seconds this player took to post a round after being +corrected.
* Type: `integer`

### stats.avgRoundLength
The average number of seconds taken to solve rounds hosted by the player.
* Type: `number`

### stats.minRoundLength
The minimum number of seconds taken to solve any round hosted by the player.
* Type: `integer`

### stats.maxRoundLength
The maximum number of seconds taken to solve any round hosted by the player.
* Type: `integer`

### stats.avgPlusCorrectDelay
The average number of seconds this player took to +correct a winning answer on a round they hosted.
* Type: `number`

### stats.minPlusCorrectDelay
The minimum number of seconds this player took to +correct a winning answer on a round they hosted.
* Type: `integer`

### stats.maxPlusCorrectDelay
The maximum number of seconds this player took to +correct a winning answer on a round they hosted.
* Type: `integer`
