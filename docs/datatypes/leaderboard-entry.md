# Leaderboard Entry

The `LeaderboardEntry` object contains information about a player on the leaderboard and the rounds they have won.

## Properties

### username
The Reddit username of the player.
* Type: `string`
* Example: `Provium`

### rank
The rank of the player, if applicable.
* Type: `integer`
* Example: `51`

### numWins
The number of rounds the player has won.
* Type: `integer`
* Example: `204`

### roundList
An array of round numbers of rounds that the player has won.
* Type: `integer[]`
* Example: `[57203, 57223, 57246, 58390, 58874]`

### winTimeList
An array of unix timestamps representing the times that the player has won rounds. Only returned if the `includeWinTimes` query parameter is `true`
* Type: `integer[]`
* Example: `[1392783968,1392786386,1393035713,1393115671,1393260675]`
