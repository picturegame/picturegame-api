# List Players

The **Players List** endpoint can be used to fetch high level stats for a given list of PictureGame players. The stats can be generated based on a filtered list of rounds, or all rounds.

**Note**: This endpoint returns a paginated list of rounds matching the search criteria. The page size limit may change at any time. If you are writing an integration with the API, please use the [pageSize](#pageSize) field on the response object to ensure you fetch all data correctly.

## Details

* Method: `GET`
* Path: `/players`

## Query Parameters

### names
**Required**. A list of usernames of players for whom to fetch stats. Can be specified in multiple different formats.
* Type: `string[]`
* Example: `?names=Provium,TheLamestUsername,quatrevingteuf`
* Example: `?names[]=Provium&names[]=TheLamestUsername&names[]=quatrevingtneuf`

### filterRounds
An expression to filter the rounds that will be taken into account when generating stats for each player. See [Filters](/guides/filters.md) for more information.
* Type: `string`
* Example: `?filterRounds=roundNumber gt 50000 and hostName eq 'provium'`

## Returns

A JSON object with the following properties:

### totalNumResults
The total number of found players. If this value is greater than [pageSize](#pageSize), you will need to perform another request using [offset](#offset) to fetch additional results.
* Type: `integer`

### startIndex
The index of the first result (zero-indexed). Equivalent to the value of [offset](#offset).
* Type: `integer`

### pageSize
The maximum number of results to be included for a single request.
* Type: `integer`

### results
The data for each player requested.
* Type: [Player](/datatypes/player.md)`[]`

