# Current Round

The **Current Round** endpoint can be used to fetch information about the current round.

## Details

* Method: `GET`
* Path: `/current`

## Returns

A [Round](/datatypes/round.md) object containing all of the information that is currently on-hand for the current round.
