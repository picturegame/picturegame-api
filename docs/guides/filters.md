# Filters

**Filters** can be used on the [Round List](/endpoints/rounds-list.md) endpoint to specify criteria for which rounds to fetch.

## Base Expression

The base expression for the filter is structured as follows:
* `Identifier` `Operator` `Value`

Where:
* `Identifier` is a property of the [Round](/datatypes/round.md) object
* `Operator` is a [logical operator](#logical-operators) relevant to the data type of the `Identifier`
* `Value` is one of:
    * A literal value of the correct type given the `Identifier` and the `Operator`, or
    * An `Identifier` for another property with the correct type.

### Examples

* `roundNumber eq 40000`
* `hostName ne "Provium"`
* `winTime gte 2019-01-01`
* `hostName eq winnerName`

## Logical Operators

For each data type, there is a set of valid operators that can be used:

### Numerical Values

| Operator | Description | Type of `Value` |
|:--|:--|:--|
| `eq` | Equals | Number |
| `ne` | Not Equals | Number |
| `gt` | Greater Than | Number |
| `gte` | Greater or Equal | Number |
| `lt` | Less Than | Number |
| `lte` | Less or Equal | Number |
| `in` | In | Number[] |
| `notin` | Not In | Number[] |

### DateTime Values

| Operator | Description | Type of `Value` |
|:--|:--|:--|
| `eq` | Equals | DateTime |
| `ne` | Not Equals | DateTime |
| `gt` | Greater Than | DateTime |
| `gte` | Greater or Equal | DateTime |
| `lt` | Less Than | DateTime |
| `lte` | Less or Equal | DateTime |

### Time Values

| Operator | Description | Type of `Value` |
|:--|:--|:--|
| `eq` | Equals | Time |
| `ne` | Not Equals | Time |
| `gt` | Greater Than | Time |
| `gte` | Greater or Equal | Time |
| `lt` | Less Than | Time |
| `lte` | Less or Equal | Time |

### String Values

| Operator | Description | Type of `Value` |
|:--|:--|:--|
| `eq` | Equals | String |
| `ne` | Not Equals | String |
| `cont` | Contains (substring) | String |
| `ncont` | Not Contains (substring) | String |
| `in` | In | String[] |
| `notin` | Not In | String[] |

### Boolean Values

| Operator | Description | Type of `Value` |
|:--|:--|:--|
| `eq` | Equals | Boolean |

## Value Types

### Number
A decimal representation of a positive integer.
* Example fields: `roundNumber`
* Example values: `100`, `12345`, `0`

### DateTime
An ISO-8601 DateTime string (time component optional).
* Example fields: `postTime`, `winTime`
* Example values: `2019-01-01`, `2018-12-25T09:30:00Z`, `2019-02-09T19:22:00+13:00`

### String
A sequence of characters, surrounded by quotes - either `""` or `''`.
* Example fields: `title`, `hostName`, `id`
* Example values: `"Provium"`, `'abc123'`

### Time
A time string in the format `hh:mm:ss`.
* Example fields: `postTimeOfDay`, `winTimeOfDay`
* Example values: `00:00:00`, `23:59:59`

### Boolean
A value that is either true or false.
* Example fields: `abandoned`
* Example values: `true`, `false`

### Array (Number[], String[])
A list of values of the given type, separated by commas and surrounded by square braces.
Used with the `in` and `notin` operators to match with any or none of a given set of values.
* Example fields: `winDayOfWeek`, `winnerName`
* Example values: `[0, 1, 2]`, `["Provium", "SuperFreakonomics"]`

## Compound Expressions

Base expressions can be linked using either `and` or `or`. Precedence rules for these two operators are the same as JavaScript.

Brackets `()` can be used to ensure the correct precedence for a particular expression.

### Examples

* `hostName eq "Provium" and winnerName eq "PM_me_cute_penguins_"`
* `(postTime gte 2019-01-01 and postTime lt 2019-02-01) or (postTime gte 2018-01-01 and postTime lt 2018-02-01)`
* `postTimeOfDay lte 12:00:00`
* `abandoned eq true`
