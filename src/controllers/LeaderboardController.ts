import { LeaderboardFilters } from '../db/RoundsTable';

import { NotFoundError } from '../models/Error';
import { LeaderboardCountResponse, LeaderboardEntry, LeaderboardResponse } from '../models/model';

import { validPositiveInteger } from '../validation/Validator';

import { BaseController } from './BaseController';

export class LeaderboardController extends BaseController {
    private invalidUsernames: string[] = [];
    private invalidRanks: string[] = [];

    async getLeaderboardSubset(filter: LeaderboardFilters): Promise<LeaderboardResponse> {
        let leaderboard = await this.db.rounds.getLeaderboard(filter);
        const startIndex = (filter.fromRank || 1) - 1;
        leaderboard = getSubset(leaderboard, startIndex, filter.count);
        leaderboard = this.filterLeaderboard(leaderboard, filter.players, filter.ranks);

        const response: LeaderboardResponse = { leaderboard };
        if (this.invalidRanks.length) {
            response.invalidRanks = this.invalidRanks;
        }
        if (this.invalidUsernames.length) {
            response.invalidUsernames = this.invalidUsernames;
        }
        return response;
    }

    async getPlayerCount(filter: LeaderboardFilters): Promise<LeaderboardCountResponse> {
        const numWinners = await this.db.rounds.getNumPlayers(filter);
        return { numWinners };
    }

    private filterLeaderboard(list: LeaderboardEntry[], usernames?: string[], ranks?: (string | number)[]) {
        if (!usernames && !ranks) {
            return list;
        }

        const filteredList = new Set<LeaderboardEntry>();

        if (usernames) {
            this.addByUsernames(list, filteredList, usernames);
        }

        if (ranks) {
            this.addByRanks(list, filteredList, ranks);
        }

        return [...filteredList].sort((a, b) => (a.rank as number) - (b.rank as number));
    }

    private addByUsernames(fullList: LeaderboardEntry[], filtered: Set<LeaderboardEntry>, usernames: string[]) {
        const playersByName = new Map<string, LeaderboardEntry>(
            fullList.map(p => [p.username.toLowerCase(), p] as [string, LeaderboardEntry]));

        usernames.forEach(u => {
            if (typeof u !== 'string') {
                this.invalidUsernames.push(`${u}`);
                return;
            }

            const player = playersByName.get(u.toLowerCase());
            if (!player) {
                this.invalidUsernames.push(u);
            } else {
                filtered.add(player);
            }
        });
    }

    private addByRanks(fullList: LeaderboardEntry[], filtered: Set<LeaderboardEntry>, ranks: (string | number)[]) {
        const playersByRank = new Map<number, LeaderboardEntry>(fullList.map(p => [p.rank, p] as [number, LeaderboardEntry]));
        ranks.forEach(r => {
            let rank: number;

            if (typeof r === 'number') {
                rank = r;
            } else if (typeof r === 'string') {
                if (!r.match(validPositiveInteger)) {
                    this.invalidRanks.push(r);
                    return;
                }
                rank = parseInt(r, 10);
            } else {
                this.invalidRanks.push(`${r}`);
                return;
            }

            const player = playersByRank.get(rank);
            if (!player) {
                this.invalidRanks.push(`${r}`);
            } else {
                filtered.add(player);
            }
        });
    }
}

function getSubset(list: LeaderboardEntry[], startIndex: number, count?: number): LeaderboardEntry[] {
    if (startIndex >= list.length) {
        throw new NotFoundError(NotFoundError.InvalidRank);
    }

    if (count) {
        return list.slice(startIndex, startIndex + count);
    }
    return list.slice(startIndex);
}
