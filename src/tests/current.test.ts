import * as request from 'supertest';

import { Application } from '../application';

import * as testUtils from './testutils';

describe('/current integration tests', () => {
    let app: Application;

    beforeAll(async () => {
        app = await testUtils.getTestServer({},
            'INSERT INTO rounds(round_number) VALUES (1);',
            'INSERT INTO rounds(round_number) VALUES (2);',
            'INSERT INTO rounds(round_number) VALUES (3);',
        );
    });

    afterAll(async () => {
        await app?.teardown();
    });

    it('should return a round object', async () => {
        const res = await request(app.app).get('/current').expect(200);
        expect(res.body.round).toBeDefined();
        expect(res.body.round.roundNumber).toBe(3);
    });
});
