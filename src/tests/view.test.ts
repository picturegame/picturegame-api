import * as request from 'supertest';

import { Application } from '../application';
import { NotFoundError, ValidationErrorCode } from '../models/Error';

import * as utils from './testutils';

describe('/view integration tests', () => {
    let app: Application;

    beforeAll(async () => {
        app = await utils.getTestServer({ api: { authBypass: true } },
            'INSERT INTO rounds(round_number, id) VALUES (1, "round1id");',
            'INSERT INTO rounds(round_number) VALUES (2);',
            'INSERT INTO rounds(round_number, id) VALUES (3, "round3id");',
        );
    });

    afterAll(async () => {
        await app?.teardown();
    });

    it('should redirect to the current round', async () => {
        const res = await request(app.app).get('/view/current').expect(302);
        expect(res.header['location']).toBe('https://redd.it/round3id');
    });

    it('should redirect to the specified round', async () => {
        const res = await request(app.app).get('/view/round/1').expect(302);
        expect(res.header['location']).toBe('https://redd.it/round1id');
    });

    it('should 404 if the round is not found', async () => {
        const res = await request(app.app).get('/view/round/4').expect(404);
        expect(res.body).toEqual(new NotFoundError(NotFoundError.RoundNotFound));
    });

    it('should 404 if the rounds id is not known', async () => {
        const res = await request(app.app).get('/view/round/2').expect(404);
        expect(res.body).toEqual(new NotFoundError(NotFoundError.UnknownRoundId));
    });

    it('should 400 if the round number is invalid', async () => {
        const res = await request(app.app).get('/view/round/a').expect(400);
        expect(res.body.data.errors
            .filter((e: string) => e === ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'))
            .length).toBe(1);
    });
});
