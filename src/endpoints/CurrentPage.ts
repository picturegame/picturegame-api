import * as express from 'express';

import { Config, Response } from '../index.d';

import { DbWrapper } from '../db';

import Endpoint from './Endpoint';

export function configureCurrent(config: Config, db: DbWrapper): express.Router {
    const endpoint = new CurrentPage(config, db);
    const router = express.Router();

    router.get('/', endpoint.getCurrent);

    return router;
}

class CurrentPage extends Endpoint {
    getCurrent = this.configureEndpoint(async (_req: express.Request, res: Response) => {
        if (this.config.test?.throwOnRequest) {
            throw new Error('Throwing an error for testing');
        }

        res.json({
            round: await this.db.rounds.getLatestRound(),
        });
    });
}
