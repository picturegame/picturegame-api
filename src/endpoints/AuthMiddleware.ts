import * as express from 'express';
import * as Passport from 'passport';

import { Response } from '../index.d';
import { UnauthorizedError } from '../models/Error';

export function authenticate(bypassAuth: boolean) {
    return function _authenticate(req: express.Request, res: Response, next: express.NextFunction) {
        if (bypassAuth) {
            return next();
        }

        Passport.authenticate('basic', { session: false }, (err, user) => {
            if (err) {
                res.locals.logger.info('Auth failed for request');
                return next(err);
            }
            if (!user) {
                res.locals.logger.info('No auth provided for request', { id: res.locals.id });
                return next(new UnauthorizedError(UnauthorizedError.AuthRequired));
            }

            res.locals.logger = res.locals.logger.child({ username: user?.username });
            res.locals.logger.info('Auth successful');
            next();
        })(req, res, next);
    };
}
