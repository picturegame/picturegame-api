import * as express from 'express';
import * as _ from 'lodash';
import { Logger } from 'winston';

import { Config, Response } from '../index.d';
import { ApiError, BadRequestError } from '../models/Error';
import * as model from '../models/model';

import { LeaderboardController } from '../controllers/LeaderboardController';
import { RoundsController } from '../controllers/RoundsController';
import { DbWrapper } from '../db';
import {
    BulkRequestValidator,
    SingleLeaderboardRequestValidator, SingleRoundsAggregateRequestValidator, SingleRoundsRequestValidator,
} from '../validation/BulkRequestValidator';

import Endpoint from './Endpoint';

export function configureBulk(config: Config, db: DbWrapper): express.Router {
    const endpoint = new BulkPage(config, db);
    const router = express.Router();

    router.route('/').post(endpoint.bulkRequest);

    return router;
}

class BulkPage extends Endpoint {
    bulkRequest = this.configureEndpoint(async (req: express.Request, res: Response) => {
        const body: model.BulkRequest = req.body;
        if (!body || !_.isObject(body) || _.isArray(body) || _.isEmpty(body)) {
            throw new BadRequestError(BadRequestError.EmptyBodyError);
        }

        new BulkRequestValidator().validateAndParse(body);

        const leaderboardController = new LeaderboardController(this.db, res.locals.logger);
        const roundsController = new RoundsController(this.db, res.locals.logger);

        const responses = await Promise.all(body.requests.map(singleReq =>
            this.doSingleRequest(singleReq, leaderboardController, roundsController, res.locals.logger)));

        const response: model.BulkResponse = { responses };
        res.json(response);
    });

    private async doSingleRequest(
        req: model.SingleRequest,
        lbController: LeaderboardController,
        roundsController: RoundsController,
        logger: Logger) {
        try {
            switch (req.endpoint) {
                case 'leaderboard':
                    return await this.doLeaderboardRequest(req as model.SingleLeaderboardRequest, lbController);
                case 'rounds':
                    return await this.doRoundsRequest(req as model.SingleRoundsRequest, roundsController);
                case 'rounds/aggregate':
                    return await this.doRoundsAggregateRequest(req as model.SingleRoundsAggregateRequest, roundsController);
                default:
                    return new BadRequestError(BadRequestError.InvalidSingleRequestType) as model.ErrorResponse;
            }
        } catch (err) {
            if (err instanceof ApiError) {
                return err as model.ErrorResponse;
            }

            const metadata = { ...err };
            if (err instanceof Error) {
                metadata.stack = err.stack;
                metadata.errorMessage = err.message;
                metadata.errorName = err.name;
            }

            logger.error('Error processing request', metadata);
            return {
                message: 'Something went wrong. Please try again or contact Provium if the issue persists.',
            } as model.ErrorResponse;
        }
    }

    private async doLeaderboardRequest(
        req: model.SingleLeaderboardRequest, controller: LeaderboardController): Promise<model.LeaderboardResponse> {

        const filters = new SingleLeaderboardRequestValidator().validateAndParse(req);
        return await controller.getLeaderboardSubset(filters);
    }

    private async doRoundsRequest(
        req: model.SingleRoundsRequest, controller: RoundsController): Promise<model.ListResponse<model.Round>> {

        const filters = new SingleRoundsRequestValidator(this.config).validateAndParse(req);
        return await controller.getRounds(filters);
    }

    private async doRoundsAggregateRequest(
        req: model.SingleRoundsAggregateRequest, controller: RoundsController): Promise<model.ListResponse<model.RoundGroup>> {

        const filters = new SingleRoundsAggregateRequestValidator(this.config).validateAndParse(req);
        return await controller.getRoundAggregates(filters);
    }
}
