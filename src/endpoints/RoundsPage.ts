import * as express from 'express';

import { Config, Response } from '../index.d';

import { RoundsController } from '../controllers';
import { DbWrapper } from '../db';
import * as Query from '../models/Query';

import { authenticate } from './AuthMiddleware';
import Endpoint from './Endpoint';

export function configureRounds(config: Config, db: DbWrapper): express.Router {
    const endpoint = new RoundsPage(config, db);
    const router = express.Router();
    const auth = authenticate(config.api.authBypass);

    router.route('/aggregate')
        .get(endpoint.getRoundAggregates);

    router.route('/:roundNumber')
        .get(endpoint.getSpecificRound)
        .patch(auth, endpoint.patchRound)
        .delete(auth, endpoint.deleteRound);

    router.route('/')
        .get(endpoint.getRounds)
        .post(auth, endpoint.postRound);

    return router;
}

class RoundsPage extends Endpoint {
    getSpecificRound = this.configureEndpoint(async (req: express.Request, res: Response) => {
        const roundsController = new RoundsController(this.db, res.locals.logger);
        const round = await roundsController.getRoundForNumber(req.params.roundNumber);
        res.json(round);
    });

    postRound = this.configureEndpoint(async (req: express.Request, res: Response) => {
        const roundsController = new RoundsController(this.db, res.locals.logger);
        const result = await roundsController.postRound(req.body);
        res.status(201).json(result);
    });

    patchRound = this.configureEndpoint(async (req: express.Request, res: Response) => {
        const roundsController = new RoundsController(this.db, res.locals.logger);
        const result = await roundsController.patchRound(req.params.roundNumber, req.body);
        res.json(result);
    });

    deleteRound = this.configureEndpoint(async (req: express.Request, res: Response) => {
        const roundsController = new RoundsController(this.db, res.locals.logger);
        const result = await roundsController.deleteRound(req.params.roundNumber);
        res.json(result);
    });

    getRounds = this.configureEndpoint(async (req, res) => {
        const roundsController = new RoundsController(this.db, res.locals.logger);
        const result = await roundsController.getRounds({
            filter: Query.extractStringOrThrow(req.query, 'filter'),
            select: Query.array(req.query, 'select'),
            sort: Query.array(req.query, 'sort'),
            ...this.getPaginationFields(req.query),
        });
        res.json(result);
    });

    getRoundAggregates = this.configureEndpoint(async (req, res) => {
        const roundsController = new RoundsController(this.db, res.locals.logger);
        const result = await roundsController.getRoundAggregates({
            filter: Query.extractStringOrThrow(req.query, 'filter'),
            select: Query.array(req.query, 'select'),
            sort: Query.array(req.query, 'groupSort'),
            groupBy: Query.extractStringOrThrow(req.query, 'groupBy'),
            groupFilter: Query.extractStringOrThrow(req.query, 'groupFilter'),
            ...this.getPaginationFields(req.query),
        });
        res.json(result);
    });

    private getPaginationFields(query: any) {
        return {
            limit: Math.min(Query.nonNegativeInt(query, 'limit') ?? this.config.api.pageSizeLimit, this.config.api.pageSizeLimit),
            offset: Query.nonNegativeInt(query, 'offset'),
        };
    }
}
