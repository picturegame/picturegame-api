import * as express from 'express';
import * as winston from 'winston';

import { Response } from '../index.d';

let id = 0;

export const assignLogger = (globalLogger: winston.Logger) =>
    (req: express.Request, res: Response, next: express.NextFunction) => {

        res.locals.startTime = Date.now();
        res.locals.id = (id++).toString(36);

        const logger = res.locals.logger = globalLogger.child({
            id: res.locals.id,
            path: req.path,
            query: req.query,
            host: req.hostname,
            method: req.method,
        });

        logger.info('Started handling request');
        next();
    };

export function logRequestEnd(_req: express.Request, res: Response, next: express.NextFunction) {
    res.locals.logger.info('Finished handling request', {
        status: res.statusCode,
        time: Date.now() - res.locals.startTime,
    });
    next();
}
