import * as express from 'express';

import { DbWrapper } from '../db';
import { Config } from '../index.d';

export default class Endpoint {
    constructor(protected config: Config, protected db: DbWrapper) {
    }

    protected configureEndpoint(handler: express.RequestHandler): express.RequestHandler {
        return (req, res, next) => {
            if (res.finished) {
                next();
            } else {
                handler(req, res, next).then(next).catch(next);
            }
        };
    }
}
