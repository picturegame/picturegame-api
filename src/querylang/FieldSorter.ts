import { getTokenTypeOrThrow, FieldSpec, QueryParseError, Tokenizer, TokenType } from 'picturegame-api-wrapper';

interface SortInfo {
    column: string;
    direction: string;
}

export function getSortInfo<T>(input: string, spec: FieldSpec<T>): SortInfo {
    const tokens = new Tokenizer(input).tokenize();
    if (tokens.length < 1 || tokens.length > 2) {
        throwForInvalidSort();
    }
    const [fieldTok, dirTok] = tokens;
    if (fieldTok.type !== TokenType.Identifier) {
        throwForInvalidSort();
    }
    if (dirTok && dirTok.type !== TokenType.DirectionOperator) {
        throwForInvalidSort();
    }
    getTokenTypeOrThrow(fieldTok, spec);
    return {
        column: fieldTok.input,
        direction: dirTok?.input ?? 'asc',
    };
}

function throwForInvalidSort() {
    throw new QueryParseError('Invalid sort: expected IDENTIFIER [DIRECTION]');
}
