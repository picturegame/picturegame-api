import * as Knex from 'knex';
import { ExpressionTreeBuilder, Tokenizer } from 'picturegame-api-wrapper';

import { RoundAggsFieldSpec, RoundAggQueryContext } from '../../db/RoundAggQueryContext';
import { RoundFieldSpec, RoundQueryContext } from '../../db/RoundQueryContext';

import { buildQuery } from '../QueryBuilder';

const TestInputs = [
    'roundNumber eq 1',
    'winTime gte 2017-01-01',
    'winnerName ne "a"',
    'winTimeOfDay lte 13:30:00',
    'abandoned eq true',
    'hostName eq "a" and winnerName eq "b"',
    'id eq "abcdef" or roundNumber lt 50000',
    'hostName eq "a" and winnerName eq "b" or winnerName eq "a" and hostName eq "b"',
    'id eq "a" and (roundNumber eq 1 or hostName eq "a")',
    'winTime lt plusCorrectTime',
    'plusCorrectDelay lt postDelay',
    'title cont "binomial"',
    'winnerName ncont "prov"',
    'winnerName in ["provium", "pm_me_cute_penguins_"]',
    'winDayOfWeek notin [0, 6]',
    'roundNumber in [40000, 50000, 60000]',
];

const TestHavingInputs = [
    'minRoundNumber eq 1',
    'maxWinTime gte 2017-01-01',
    'avgSolveTime lt 600',
    'numRounds in [100, 200, 300]',
    'minPlusCorrectDelay ne 100',
    'maxPostDelay lte 2600',
    'maxRoundNumber notin [50000, 60000]',
    'minRoundNumber gt 1000 and maxRoundNumber lt 2000',
];

const InvalidDates = [
    '2022-13-01',
    '2022-00-01',
    '2022-01-00',
    '2022-01-32',
    '2020-02-30',
    '2021-02-29',
];

describe('QueryBuilder', () => {
    let knex: Knex;
    beforeAll(() => {
        knex = Knex({ client: 'sqlite3', useNullAsDefault: true });
    });

    for (const input of TestInputs) {
        test(input, () => {
            const context = new RoundQueryContext(knex);
            const tokens = new Tokenizer(input).tokenize();
            const tree = new ExpressionTreeBuilder(tokens).read();
            buildQuery(tree, context, RoundFieldSpec, 'where');
            expect(context.toString()).toMatchSnapshot();
        });
    }

    describe('group filter/having', () => {
        for (const input of TestHavingInputs) {
            test(input, () => {
                const rContext = new RoundQueryContext(knex);
                const context = new RoundAggQueryContext(rContext);
                const tokens = new Tokenizer(input).tokenize();
                const tree = new ExpressionTreeBuilder(tokens).read();
                buildQuery(tree, context, RoundAggsFieldSpec, 'having');
                expect(context.toString()).toMatchSnapshot();
            });
        }
    });

    describe('throw an exception for an invalid date', () => {
        for (const input of InvalidDates) {
            test(input, () => {
                const context = new RoundQueryContext(knex);
                const tokens = new Tokenizer(`postTime eq ${input}`).tokenize();
                const tree = new ExpressionTreeBuilder(tokens).read();
                buildQuery(tree, context, RoundFieldSpec, 'where');
                let threw = false;
                try {
                    context.toString();
                } catch (e) {
                    threw = true;
                    expect(e).toMatchSnapshot();
                } finally {
                    expect(threw).toBe(true);
                }
            });
        }
    });
});
