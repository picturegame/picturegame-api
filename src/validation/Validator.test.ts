import { ValidationErrorCode } from '../models/Error';

import { RoundValidator } from './RoundValidator';

describe('Round validation unit tests', () => {

    describe('#roundNumber', () => {

        it('should fail if roundNumber is missing', () => {
            const errors = getValidationErrors({});
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'));
        });

        it('should fail if roundNumber is a negative integer', () => {
            const errors = getValidationErrors({ roundNumber: -9 });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'));
        });

        it('should fail if roundNumber is a float', () => {
            const errors = getValidationErrors({ roundNumber: 1.5 });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'));
        });

        it('should fail if roundNumber cannot be parsed as an int', () => {
            let errors = getValidationErrors({ roundNumber: 'a' } as any);
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'));

            errors = getValidationErrors({ roundNumber: {} } as any);
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'));

            errors = getValidationErrors({ roundNumber: [] } as any);
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'));
        });

        it('should succeed if roundNumber is an int', () => {
            const errors = getValidationErrors({ roundNumber: 1 });
            expect(errors.length).toBe(0);

            const r = new RoundValidator().parse({ roundNumber: 1 });
            expect(r.roundNumber).toBe(1);
        });

        it('should succeed if roundNumber is a string representation of an int', () => {
            const errors = getValidationErrors({ roundNumber: '1' } as any);
            expect(errors.length).toBe(0);

            const r = new RoundValidator().parse({ roundNumber: '1' } as any);
            expect(r.roundNumber).toBe(1);
        });
    });

    describe('#usernames', () => {

        it('should fail if username is not a string', () => {
            const errors = getValidationErrors({
                roundNumber: 1,
                hostName: 1,
                winnerName: {},
            } as any);

            expect(errors.length).toBe(2);
            expect(errors[0]).toBe(ValidationErrorCode.UsernameMustBeValid);
            expect(errors[1]).toBe(ValidationErrorCode.UsernameMustBeValid);
        });

        it('should fail if username is the empty string', () => {
            const errors = getValidationErrors({ roundNumber: 1, hostName: '' });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.UsernameMustBeValid);
        });

        it('should fail if username contains forbidden characters', () => {
            const errors = getValidationErrors({ roundNumber: 1, hostName: '(' });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.UsernameMustBeValid);
        });

        it('should fail if username is too long', () => {
            const errors = getValidationErrors({ roundNumber: 1, hostName: 'aaaaaaaaaaaaaaaaaaaaa' });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.UsernameMustBeValid);
        });

        it('should succeed if username is valid', () => {
            const errors = getValidationErrors({ roundNumber: 1, hostName: 'Provium' });
            expect(errors.length).toBe(0);

            const r = new RoundValidator().parse({ roundNumber: 1, hostName: 'Provium' });
            expect(r.hostName).toBe('Provium');
        });

        it('should fail if winnerName and hostName are the same', () => {
            const errors = getValidationErrors({
                roundNumber: 1,
                hostName: 'Provium',
                winnerName: 'Provium',
            });

            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.RoundCannotBeWonByHost);
        });
    });

    describe('#timeWindows', () => {

        it('should fail if hostTime is not before winTime', () => {
            let errors = getValidationErrors({
                roundNumber: 1,
                postTime: 10,
                winTime: 10,
            });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.RoundMustEndAfterItBegins);

            errors = getValidationErrors({
                roundNumber: 1,
                postTime: 11,
                winTime: 10,
            });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.RoundMustEndAfterItBegins);
        });

        it('should give the correct error if timestamps are not valid', () => {
            const errors = getValidationErrors({ roundNumber: 1, postTime: -1 });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.ValueMustBePositiveInteger('postTime'));
        });

        it('should fail if postTime is not before plusCorrectTime', () => {
            const errors = getValidationErrors({
                roundNumber: 1,
                postTime: 10,
                plusCorrectTime: 9,
            });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.RoundCannotBeCorrectedBeforeStart);
        });

        it('should fail if winTime is not before plusCorrectTime', () => {
            const errors = getValidationErrors({
                roundNumber: 1,
                winTime: 10,
                plusCorrectTime: 9,
            });
            expect(errors.length).toBe(1);
            expect(errors[0]).toBe(ValidationErrorCode.RoundCannotBeCorrectedBeforeAnswer);
        });
    });
});

function getValidationErrors(round: any) {
    const v = new RoundValidator();
    v.validate(round);
    return v.errors;
}
