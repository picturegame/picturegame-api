import * as Bluebird from 'bluebird';
import * as Knex from 'knex';
import * as path from 'path';
import * as winston from 'winston';

import { MetaTable } from './MetaTable';
import { RoundsTable } from './RoundsTable';
import { StatusTable } from './StatusTable';
import { UsersTable } from './UsersTable';

// Log config isn't included in Knex typings :(
interface KnexConfig extends Knex.Config {
    log?: {
        warn(message: string): void;
        error(message: string): void;
        deprecate(message: string): void;
        debug(message: string): void;
    };
}

function createDatabase(filename?: string, logger?: winston.Logger) {
    const config: KnexConfig = {
        client: 'sqlite3',
        connection: { filename: filename ?? defaultFileName },
        useNullAsDefault: true,
    };
    if (logger) {
        config.log = {
            debug: logger.debug,
            deprecate: logger.warn,
            error: logger.error,
            warn: logger.warn,
        };
    }
    return Knex(config);
}

const defaultFileName = path.join(__dirname, '..', '..', 'picturegame.db');

export class DbWrapper {
    private _db: Knex;
    private _closingDbPromise: Bluebird<void>;
    private _logger?: winston.Logger;

    public meta: MetaTable;
    public rounds: RoundsTable;
    public status: StatusTable;
    public users: UsersTable;

    constructor(private version: string, filename?: string, logger?: winston.Logger) {
        this._logger = logger;
        this._db = createDatabase(filename, logger);
    }

    static async setup(version: string, filename?: string, logger?: winston.Logger) {
        const instance = new DbWrapper(version, filename, logger);
        await instance.createTables();
        return instance;
    }

    public async close() {
        if (!this._closingDbPromise) {
            this._closingDbPromise = this._db.destroy();
        }
        await this._closingDbPromise;
    }

    public async createTables() {
        this.meta = new MetaTable(this._db, this._logger);
        await this.meta.createTable();

        this.rounds = new RoundsTable(this._db, this._logger);
        await this.rounds.createTable();
        await this.rounds.updateTable();
        await this.rounds.createView();

        this.users = new UsersTable(this._db, this._logger);
        await this.users.createTable();

        this.status = new StatusTable(this._db, this._logger);
        await this.status.createTable();
        await this.status.setStatus({
            name: 'picturegame-api',
            startTime: Math.floor(Date.now() / 1000),
            version: this.version,
        });
    }

    public async execSql(sql: string[]) {
        for (const query of sql) {
            await this._db.raw(query);
        }
    }
}
