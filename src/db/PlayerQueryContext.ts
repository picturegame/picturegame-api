import * as Knex from 'knex';

import { Player, RoundAggregates, RoundGroup } from '../models/model';
import * as QueryLang from '../querylang';

import { RoundFields } from './model';
import { RoundAggQueryContext } from './RoundAggQueryContext';
import { RoundFieldSpec, RoundQueryContext } from './RoundQueryContext';

const SelectHostAggFields: ReadonlyArray<keyof RoundAggregates> = [
    'avgSolveTime', 'minSolveTime', 'maxSolveTime',
    'avgPostDelay', 'minPostDelay', 'maxPostDelay',
    'avgPlusCorrectDelay', 'minPlusCorrectDelay', 'maxPlusCorrectDelay',
];

const SelectWinnerAggFields: ReadonlyArray<keyof RoundAggregates> = [
    'avgSolveTime', 'minSolveTime', 'maxSolveTime',
];

export class PlayerQueryContext {
    private _byHostRoundQuery: RoundQueryContext;
    private _byHostAggQuery: RoundAggQueryContext;

    private _byWinnerRoundQuery: RoundQueryContext;
    private _byWinnerAggQuery: RoundAggQueryContext;

    constructor(private db: Knex) {
        this._byHostRoundQuery = new RoundQueryContext(db);
        this._byHostAggQuery = new RoundAggQueryContext(this._byHostRoundQuery);
        this._byHostAggQuery.groupBy('hostName');
        for (const field of SelectHostAggFields) {
            this._byHostAggQuery.selectColumn(field);
        }

        this._byWinnerRoundQuery = new RoundQueryContext(db);
        this._byWinnerAggQuery = new RoundAggQueryContext(this._byWinnerRoundQuery);
        this._byWinnerAggQuery.groupBy('winnerName');
        for (const field of SelectWinnerAggFields) {
            this._byWinnerAggQuery.selectColumn(field);
        }
    }

    public filterRounds(filter: string) {
        QueryLang.Filter(filter, this._byHostRoundQuery, RoundFieldSpec);
        QueryLang.Filter(filter, this._byWinnerRoundQuery, RoundFieldSpec);
    }

    public filterPlayers(names: string[]) {
        this._byHostRoundQuery.query.whereIn(RoundFields.HostName, names);
        this._byWinnerRoundQuery.query.whereIn(RoundFields.WinnerName, names);
    }

    public async execute(): Promise<Player[]> {
        const [hostAggs, winnerAggs] = await Promise.all([this._byHostAggQuery.execute(), this._byWinnerAggQuery.execute()]);

        const hostAggsLookup = new Map(hostAggs.results.map(r => [(r as RoundGroup).hostName!, r]));

        const results: Player[] = [];
        for (const winnerAgg of (winnerAggs.results as RoundGroup[])) {
            const hostAgg = hostAggsLookup.get(winnerAgg.winnerName!);

            results.push({
                username: winnerAgg.winnerName!,
                stats: {
                    numRoundsWon: winnerAgg.numRounds,
                    numRoundsHosted: hostAgg?.numRounds ?? 0,

                    avgSolveTime: winnerAgg.avgSolveTime!,
                    minSolveTime: winnerAgg.minSolveTime!,
                    maxSolveTime: winnerAgg.maxSolveTime!,

                    avgPlusCorrectDelay: hostAgg?.avgPlusCorrectDelay,
                    minPlusCorrectDelay: hostAgg?.minPlusCorrectDelay,
                    maxPlusCorrectDelay: hostAgg?.maxPlusCorrectDelay,

                    avgPostDelay: hostAgg?.avgPostDelay,
                    minPostDelay: hostAgg?.minPostDelay,
                    maxPostDelay: hostAgg?.maxPostDelay,

                    avgRoundLength: hostAgg?.avgSolveTime,
                    minRoundLength: hostAgg?.minSolveTime,
                    maxRoundLength: hostAgg?.maxSolveTime,
                },
            });
        }

        return results;
    }
}
