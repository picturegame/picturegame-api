import * as Knex from 'knex';
import * as winston from 'winston';

import * as Api from '../models/model';

import { fromDbStatus, toDbStatus, Status, StatusFields, TableName } from './model';

export class StatusTable {
    constructor(private db: Knex, private logger?: winston.Logger) {
    }

    public async createTable() {
        const logger = this.logger;
        logger?.info('Creating status table');

        const exists = await this.db.schema.hasTable(TableName.Status);
        if (exists) {
            logger?.info('Status table already exists');
            return;
        }

        await this.db.schema.createTable(TableName.Status, table => {
            table.text(StatusFields.ServiceName).notNullable().primary();
            table.text(StatusFields.Version).notNullable();
            table.integer(StatusFields.StartTime).notNullable();
        });

        logger?.info('Status table created successfully');
    }

    public async setStatus(apiStatus: Api.ServiceStatusRequest) {
        const dbStatus = toDbStatus(apiStatus);

        const existing: any[] = await this.db(TableName.Status)
            .where(StatusFields.ServiceName, dbStatus[StatusFields.ServiceName]);

        if (existing?.length) {
            await this.db(TableName.Status)
                .update(dbStatus)
                .where(StatusFields.ServiceName, dbStatus[StatusFields.ServiceName]);
        } else {
            await this.db(TableName.Status).insert(dbStatus);
        }

        return fromDbStatus(dbStatus);
    }

    public async getAllStatus() {
        const res = await this.db(TableName.Status) as Status[];
        return res.map(fromDbStatus);
    }
}
