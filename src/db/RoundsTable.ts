import * as Knex from 'knex';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as winston from 'winston';

import { NotFoundError } from '../models/Error';
import * as Model from '../models/model';
import * as QueryLang from '../querylang';
import { dayPerWeek, monthPerYear, secPerDay, secPerHour, Thursday } from '../utils/TimeUtils';

import { toDbRound, Round, RoundFields, TableName } from './model';
import { PlayerQueryContext } from './PlayerQueryContext';
import { RoundAggsFieldSpec, RoundAggQueryContext } from './RoundAggQueryContext';
import { RoundFieldSpec, RoundQueryContext } from './RoundQueryContext';
import { UpdateableTable } from './UpdateableTable';

export interface LeaderboardFilters {
    fromRank?: number;
    count?: number;
    players?: string[];
    ranks?: (string | number)[];

    fromRound?: number;
    toRound?: number;

    fromTime?: moment.Moment;
    toTime?: moment.Moment;

    filterRounds?: string;

    includeRoundNumbers?: boolean;
    includeWinTimes?: boolean;
}

export interface RoundFilters {
    filter?: string;
    select?: string[];
    sort?: string[];

    limit: number;
    offset?: number;
}

export interface RoundAggregatesFilters extends RoundFilters {
    groupBy?: string;
    groupFilter?: string;
}

export interface PlayerMetricsFilters {
    filterRounds?: string;
    players: string[];
}

const CreateViewSql = `
CREATE VIEW ${TableName.RoundsView} AS SELECT

c.round_number as round_number,

c.title as title,
c.post_url as post_url,
c.thumbnail_url as thumbnail_url,
c.id as id,
c.winning_comment_id as winning_comment_id,

c.host_name as host_name,
c.post_time as post_time,

c.winner_name as winner_name,
c.win_time as win_time,
c.plus_correct_time as plus_correct_time,
c.abandoned_time as abandoned_time,

c.post_time - p.plus_correct_time as post_delay,
c.plus_correct_time - c.win_time as plus_correct_delay,
c.win_time - c.post_time as solve_time,

c.post_time % ${secPerDay} as post_time_of_day,
cast(strftime('%m', c.post_time, 'unixepoch') as int) as post_month_of_year,
cast(strftime('%W', c.post_time, 'unixepoch') as int) as post_week_of_year,
cast(strftime('%w', c.post_time, 'unixepoch') as int) as post_day_of_week,
cast(date(c.post_time, 'unixepoch') as int) as post_year,
(date(c.post_time, 'unixepoch') - date(0, 'unixepoch')) * ${monthPerYear} + strftime('%m', c.post_time, 'unixepoch') - 1 as post_month,
(c.post_time + ${Thursday} * ${secPerDay}) / (${secPerDay} * ${dayPerWeek}) as post_week,
c.post_time / ${secPerDay} as post_day,
c.post_time / ${secPerHour} as post_hour,

c.win_time % ${secPerDay} as win_time_of_day,
cast(strftime('%m', c.win_time, 'unixepoch') as int) as win_month_of_year,
cast(strftime('%W', c.win_time, 'unixepoch') as int) as win_week_of_year,
cast(strftime('%w', c.win_time, 'unixepoch') as int) as win_day_of_week,
cast(date(c.win_time, 'unixepoch') as int) as win_year,
(date(c.win_time, 'unixepoch') - date(0, 'unixepoch')) * ${monthPerYear} + strftime('%m', c.win_time, 'unixepoch') - 1 as win_month,
(c.win_time + ${Thursday} * ${secPerDay}) / (${secPerDay} * ${dayPerWeek}) as win_week,
c.win_time / ${secPerDay} as win_day,
c.win_time / ${secPerHour} as win_hour,

((c.win_time is not null and n.post_time is not null and c.win_time > n.post_time)
or (c.win_time is null and n.post_time is not null)) as abandoned

FROM rounds as c

LEFT JOIN rounds as p on
p.round_number = c.round_number - 1 and
c.host_name = p.winner_name and
c.post_time > p.plus_correct_time

LEFT JOIN rounds as n on
n.round_number = c.round_number + 1
`;

const CurrentVersion = 4;
export class RoundsTable extends UpdateableTable {
    constructor(protected db: Knex, logger?: winston.Logger) {
        super(db, TableName.Rounds, logger);
    }

    public async createTable() {
        const logger = this.logger;
        logger?.info('Creating rounds table + indexes');

        const exists = await this.db.schema.hasTable(TableName.Rounds);
        if (exists) {
            logger?.info('Rounds table already exists');
            return;
        }

        await this.db.schema.createTable(TableName.Rounds, table => {
            table.integer(RoundFields.RoundNumber).notNullable().primary();
            table.text(RoundFields.PostUrl);
            table.text(RoundFields.Id);
            table.text(RoundFields.WinningCommentId);
            table.specificType(RoundFields.HostName, 'TEXT COLLATE NOCASE');
            table.integer(RoundFields.PostTime);
            table.specificType(RoundFields.WinnerName, 'TEXT COLLATE NOCASE');
            table.integer(RoundFields.WinTime);

            table.index([RoundFields.WinnerName], 'rounds_by_winner');
            table.index([RoundFields.HostName], 'rounds_by_host');
            table.index([RoundFields.PostTime], 'rounds_by_start_time');
            table.index([RoundFields.WinTime], 'rounds_by_end_time');
        });

        logger?.info('Rounds table and indexes created successfully');
    }

    public async createView() {
        const logger = this.logger;
        logger?.info('Creating rounds view');

        await this.db.schema.raw(`DROP VIEW IF EXISTS ${TableName.RoundsView}`);
        await this.db.schema.raw(CreateViewSql);

        logger?.info('Rounds view created successfully');
    }

    public async updateTable() {
        const logger = this.logger;
        logger?.info('Running rounds table updates');

        const version = await this.getVersion();
        if (version === CurrentVersion) {
            logger?.info('Rounds table up to date');
            return;
        }

        if (version < 1) {
            logger?.info('Adding +correct time and title columns');
            await this.db.schema.alterTable(TableName.Rounds, table => {
                table.integer(RoundFields.PlusCorrectTime);
                table.text(RoundFields.Title);
            });
        }

        if (version < 2) {
            logger?.info('Adding rounds_by_submission_id index');
            await this.db.schema.alterTable(TableName.Rounds, table => {
                table.index([RoundFields.Id], 'rounds_by_submission_id');
            });
        }

        if (version < 3) {
            logger?.info('Adding thumbnail_url column');
            await this.db.schema.alterTable(TableName.Rounds, table => {
                table.text(RoundFields.ThumbnailUrl);
            });
        }

        if (version < 4) {
            logger?.info('Adding abandoned_time column');
            await this.db.schema.alterTable(TableName.Rounds, table => {
                table.integer(RoundFields.AbandonedTime);
            });
        }

        await this.setVersion(CurrentVersion);
    }

    async getLatestRound(): Promise<Model.Round> {
        const context = new RoundQueryContext(this.db);

        context.selectAll();
        const innerQuery = this.db(TableName.Rounds).max(RoundFields.RoundNumber);
        context.query.where(context.getColumnName('roundNumber'), '=', innerQuery);

        return (await context.execute()).results[0];
    }

    async getSpecificRound(roundNumber: number): Promise<Model.Round | null> {
        const context = new RoundQueryContext(this.db);

        context.selectAll();
        context.query.where(context.getColumnName('roundNumber'), '=', roundNumber);

        const round = (await context.execute()).results[0];
        return round ?? null;
    }

    async getRounds(input: RoundFilters): Promise<QueryLang.QueryResult<Model.Round>> {
        const context = new RoundQueryContext(this.db);
        if (input.filter) {
            QueryLang.Filter(input.filter, context, RoundFieldSpec);
        }
        QueryLang.Select(input.select ?? [], context, RoundFieldSpec);
        QueryLang.Sort(input.sort ?? [], context, RoundFieldSpec);

        return await context.execute(input.limit, input.offset);
    }

    async createRound(round: Model.Round) {
        const dbRound = toDbRound(round);
        await this.db(TableName.Rounds).insert(dbRound);
    }

    async batchCreateRounds(rounds: Model.Round[]) {
        const logger = this.logger;
        logger?.info(`Beginning batch insert of ${rounds.length} rounds`);

        await this.db.transaction(async trx => {
            let progress = 0;
            for (const round of rounds) {
                const dbRound = toDbRound(round);
                await trx(TableName.Rounds).insert(dbRound);

                if (++progress % 1000 === 0) {
                    logger?.debug('Progress:', progress);
                }
            }
        });

        logger?.info('Batch insert complete');
    }

    async batchUpdateRounds(rounds: Model.Round[]) {
        const logger = this.logger;
        logger?.info(`Beginning batch update of ${rounds.length} rounds`);

        await this.db.transaction(async trx => {
            let progress = 0;

            for (const round of rounds) {
                const dbRound = toDbRound(round);
                const filtered = _.pickBy(dbRound, (value) => value !== null);

                await trx(TableName.Rounds)
                    .where(RoundFields.RoundNumber, round.roundNumber)
                    .update(filtered);

                if (++progress % 100 === 0) {
                    process.stdout.write(`\rProgress: ${progress}`);
                }
            }
        });

        console.log(`\rProgress: ${rounds.length}`);
        logger?.info('Batch update complete.');
    }

    async updateRound(round: Model.Round) {
        const dbRound = toDbRound(round);
        await this.db(TableName.Rounds)
            .update(dbRound)
            .where(RoundFields.RoundNumber, round.roundNumber);
    }

    async getRoundsForWinner(winnerName: string): Promise<Set<number>> {
        const rawRounds = await this.db(TableName.Rounds)
            .select(RoundFields.RoundNumber)
            .where(RoundFields.WinnerName, winnerName) as Round[];

        return new Set<number>(rawRounds.map(r => r[RoundFields.RoundNumber]));
    }

    async deleteRound(roundNumber: number) {
        const round = await this.getSpecificRound(roundNumber);
        if (!round) {
            throw new NotFoundError(NotFoundError.RoundNotFound);
        }

        await this.db(TableName.Rounds)
            .delete()
            .where(RoundFields.RoundNumber, roundNumber);

        return round;
    }

    async renameUser(fromName: string, toName: string) {
        const result = await this.db(TableName.Rounds)
            .count(`${RoundFields.RoundNumber} as num_wins`)
            .where(RoundFields.WinnerName, fromName)
            .orWhere(RoundFields.HostName, fromName);

        if (result[0].num_wins === 0) {
            throw new NotFoundError(NotFoundError.PlayerNotFound);
        }

        await this.db.transaction(async trx => {
            await trx(TableName.Rounds).update(RoundFields.WinnerName, toName).where(RoundFields.WinnerName, fromName);
            await trx(TableName.Rounds).update(RoundFields.HostName, toName).where(RoundFields.HostName, fromName);
        });
    }

    async getLeaderboard(filter: LeaderboardFilters): Promise<Model.LeaderboardEntry[]> {
        const query = this.createLeaderboardQuery(filter)
            .select(RoundFields.WinnerName)
            .count(`${RoundFields.RoundNumber} as num_wins`)
            .max(`${RoundFields.WinTime} as last_win`)
            .whereNotNull(RoundFields.WinnerName)
            .groupBy(RoundFields.WinnerName)
            .orderBy('num_wins', 'desc')
            .orderBy('last_win', 'asc');

        if (filter.includeRoundNumbers) {
            query.select(this.db.raw(`group_concat(${RoundFields.RoundNumber}) as wins`));
        }

        if (filter.includeWinTimes) {
            query.select(this.db.raw(`group_concat(${RoundFields.WinTime}) as win_times`));
        }

        queryByRoundNumberRange(query, filter.fromRound, filter.toRound);
        queryByWinTimeRange(query, filter.fromTime, filter.toTime);

        const rows = await query as any[];

        return rows.map((row, index): Model.LeaderboardEntry => ({
            rank: index + 1,
            username: row[RoundFields.WinnerName],
            numWins: row.num_wins,
            roundList: (row.wins as string | undefined)?.split(',').map((v: string) => parseInt(v, 10)),
            winTimeList: (row.win_times as string | undefined)?.split(',').map((v: string) => parseInt(v, 10)).sort((a, b) => a - b),
        }));
    }

    async getNumPlayers(filters: LeaderboardFilters): Promise<number> {
        const query = this.createLeaderboardQuery(filters)
            .countDistinct(`${RoundFields.WinnerName} as num_winners`)
            .whereNotNull(RoundFields.WinnerName);

        queryByRoundNumberRange(query, filters.fromRound, filters.toRound);
        queryByWinTimeRange(query, filters.fromTime, filters.toTime);

        const result = await query as any[];
        return result[0].num_winners;
    }

    private createLeaderboardQuery(filter: LeaderboardFilters) {
        if (filter.filterRounds) {
            const context = new RoundQueryContext(this.db);
            QueryLang.Filter(filter.filterRounds, context, RoundFieldSpec);
            return context.query;
        }
        return this.db(TableName.Rounds);
    }

    async dumpRoundTitles(fromTime: moment.Moment, toTime: moment.Moment) {
        const query = this.db(TableName.Rounds)
            .whereNotNull(RoundFields.Title)
            .select(RoundFields.Title);

        queryByStartTimeRange(query, fromTime, toTime);

        const result = await query as Round[];
        return result.map(r => r[RoundFields.Title]) as string[];
    }

    async getRoundAggregates(input: RoundAggregatesFilters) {
        const roundContext = new RoundQueryContext(this.db);
        const aggContext = new RoundAggQueryContext(roundContext);
        if (input.filter) {
            QueryLang.Filter(input.filter, roundContext, RoundFieldSpec);
        }
        QueryLang.Select(input.select ?? [], aggContext, RoundAggsFieldSpec);

        if (input.groupBy) {
            QueryLang.GroupBy(input.groupBy as keyof Model.Round, aggContext);

            if (input.groupFilter) {
                QueryLang.Filter(input.groupFilter, aggContext, RoundAggsFieldSpec, 'having');
            }

            QueryLang.Sort(input.sort ?? [], aggContext, RoundAggsFieldSpec);
        }

        return await aggContext.execute(input.limit, input.offset);
    }

    async getPlayerMetrics(input: PlayerMetricsFilters) {
        const context = new PlayerQueryContext(this.db);
        if (input.filterRounds) {
            context.filterRounds(input.filterRounds);
        }
        context.filterPlayers(input.players);

        return await context.execute();
    }
}

function queryByRoundNumberRange(query: Knex.QueryBuilder, min?: number, max?: number) {
    queryNumericRange(query, RoundFields.RoundNumber, min, max);
}

function queryByStartTimeRange(query: Knex.QueryBuilder, min?: moment.Moment, max?: moment.Moment) {
    queryByTimeRange(query, RoundFields.PostTime, min, max);
}

function queryByWinTimeRange(query: Knex.QueryBuilder, min?: moment.Moment, max?: moment.Moment) {
    queryByTimeRange(query, RoundFields.WinTime, min, max);
}

function queryByTimeRange(query: Knex.QueryBuilder, field: RoundFields, min?: moment.Moment, max?: moment.Moment) {
    const minSeconds = moment.isMoment(min) ? min.unix() : undefined;
    const maxSeconds = moment.isMoment(max) ? max.unix() : undefined;
    queryNumericRange(query, field, minSeconds, maxSeconds);
}

function queryNumericRange(query: Knex.QueryBuilder, field: RoundFields, min?: number, max?: number) {
    if (_.isNumber(min)) {
        if (_.isNumber(max)) {
            query.whereBetween(field, [min, max]);
        } else {
            query.where(field, '>=', min);
        }
    } else if (_.isNumber(max)) {
        query.where(field, '<=', max);
    }
}
