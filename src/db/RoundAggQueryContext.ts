import * as Knex from 'knex';
import { ColumnLookup, FieldSpec, QueryParseError, TokenType } from 'picturegame-api-wrapper';

import { Round, RoundAggregates, RoundGroup } from '../models/model';
import { GroupByFieldType, GroupBySpec, GroupByTimeUnit, GroupQueryContext } from '../querylang/QueryBuilder';
import { secPerDay, secPerHour, secPerMinute, MonthToUnixTimestamp, WeekToUnixTimestamp, YearToUnixTimestamp } from '../utils/TimeUtils';

import { RoundFields, RoundViewFields } from './model';
import { RoundQueryContext } from './RoundQueryContext';

const RoundAggsColumnLookup: ColumnLookup<RoundAggregates> = {
    numRounds: 'count(*)',

    minRoundNumber: `min(${RoundFields.RoundNumber})`,
    maxRoundNumber: `max(${RoundFields.RoundNumber})`,
    allRoundNumbers: `group_concat(${RoundFields.RoundNumber})`,

    minPostTime: `min(${RoundFields.PostTime})`,
    maxPostTime: `max(${RoundFields.PostTime})`,

    minWinTime: `min(${RoundFields.WinTime})`,
    maxWinTime: `max(${RoundFields.WinTime})`,
    allWinTimes: `group_concat(ifnull(${RoundFields.WinTime}, 0))`,

    avgPostDelay: `avg(${RoundViewFields.PostDelay})`,
    minPostDelay: `min(${RoundViewFields.PostDelay})`,
    maxPostDelay: `max(${RoundViewFields.PostDelay})`,

    avgSolveTime: `avg(${RoundViewFields.SolveTime})`,
    minSolveTime: `min(${RoundViewFields.SolveTime})`,
    maxSolveTime: `max(${RoundViewFields.SolveTime})`,

    avgPlusCorrectDelay: `avg(${RoundViewFields.PlusCorrectDelay})`,
    minPlusCorrectDelay: `min(${RoundViewFields.PlusCorrectDelay})`,
    maxPlusCorrectDelay: `max(${RoundViewFields.PlusCorrectDelay})`,

    numHosts: `count(distinct ${RoundFields.HostName})`,
    numWinners: `count(distinct ${RoundFields.WinnerName})`,
};

export const RoundAggsFieldSpec: FieldSpec<RoundAggregates> = {
    numRounds: TokenType.NumericValue,

    minRoundNumber: TokenType.NumericValue,
    maxRoundNumber: TokenType.NumericValue,
    allRoundNumbers: TokenType.Invalid,

    minPostTime: TokenType.DateTimeValue,
    maxPostTime: TokenType.DateTimeValue,

    minWinTime: TokenType.DateTimeValue,
    maxWinTime: TokenType.DateTimeValue,
    allWinTimes: TokenType.Invalid,

    avgPostDelay: TokenType.NumericValue,
    minPostDelay: TokenType.NumericValue,
    maxPostDelay: TokenType.NumericValue,

    avgSolveTime: TokenType.NumericValue,
    minSolveTime: TokenType.NumericValue,
    maxSolveTime: TokenType.NumericValue,

    avgPlusCorrectDelay: TokenType.NumericValue,
    minPlusCorrectDelay: TokenType.NumericValue,
    maxPlusCorrectDelay: TokenType.NumericValue,

    numHosts: TokenType.NumericValue,
    numWinners: TokenType.NumericValue,
};

const RoundGroupBySpec: GroupBySpec<Round> = {
    hostName: GroupByFieldType.ExactMatch,
    winnerName: GroupByFieldType.ExactMatch,
    postDayOfWeek: GroupByFieldType.ExactMatch,
    winDayOfWeek: GroupByFieldType.ExactMatch,

    postTime: GroupByFieldType.AbsoluteTime,
    winTime: GroupByFieldType.AbsoluteTime,

    postTimeOfDay: GroupByFieldType.RelativeTime,
    winTimeOfDay: GroupByFieldType.RelativeTime,

    postDelay: GroupByFieldType.Duration,
    solveTime: GroupByFieldType.Duration,
    plusCorrectDelay: GroupByFieldType.Duration,
};

export class RoundAggQueryContext extends GroupQueryContext<RoundAggregates, Round> {
    private columns: { [K in keyof RoundAggregates]?: Knex.Raw } = {};

    constructor(private roundContext: RoundQueryContext) {
        // We share a query instance with the RoundQueryContext, so that we can filter the individual rounds
        super(roundContext.db, roundContext.query);
        this.selectColumn('numRounds');
    }

    getColumnName(field: keyof RoundAggregates): string | Knex.Raw {
        if (field in this.columns) {
            return field;
        }
        return this.db.raw(RoundAggsColumnLookup[field]);
    }

    selectColumn(field: keyof RoundAggregates): void {
        const sql = RoundAggsColumnLookup[field];
        this.columns[field] = this.db.raw(sql);
    }

    protected convert(dbAgg: any): RoundGroup {
        const group: RoundGroup = {
            ...dbAgg,
            allWinTimes: (dbAgg.allWinTimes as string)?.split(',').map(n => parseInt(n, 10)).sort((a, b) => a - b),
            allRoundNumbers: (dbAgg.allRoundNumbers as string)
                ?.split(',').map(n => parseInt(n, 10)).sort((a, b) => a - b),
        };
        if (this.groupByCol) {
            group[this.groupByCol.field] = this.formatGroupKey(group[this.groupByCol.field]);
        }
        return group;
    }

    async count(): Promise<number> {
        if (!this.groupByCol) {
            // Not grouped, so we're just returning the overall aggs
            return 1;
        }
        return await super.count();
    }

    protected prepare(): void {
        super.prepare();

        const columns: any = { ...this.columns };
        if (this.groupByCol) {
            columns[this.groupByCol.field] = this.db.raw(this.groupByCol.sql);
            this.query.groupBy(this.groupByCol.field);
            this.query.whereNotNull(this.groupByCol.field);
        }

        this.query.column(columns);
    }

    protected getExactMatchGroupBySql(field: keyof Round): string {
        return this.roundContext.getColumnName(field);
    }

    protected getAbsoluteTimeGroupBySql(field: keyof Round, binSize: number, unit: GroupByTimeUnit): string {
        // field is either postTime or winTime; no other fields support absolute time grouping
        const prefix = field === 'postTime' ? 'post' : 'win';

        switch (unit) {
            case GroupByTimeUnit.Year:
            case GroupByTimeUnit.Month:
            case GroupByTimeUnit.Week:
            case GroupByTimeUnit.Day:
            case GroupByTimeUnit.Hour:
                return `${prefix}_${unit}` + (binSize > 1 ? ` / ${binSize} * ${binSize}` : '');
            default:
                throw new QueryParseError(`Invalid time unit for groupBy: ${unit}. Expected one of year, month, week, day, hour.`);
        }
    }

    protected getRelativeTimeGroupBySql(field: keyof Round, binSize: number, unit: GroupByTimeUnit): string {
        const column = this.roundContext.getColumnName(field);
        let binSizeSecs: number;

        switch (unit) {
            case GroupByTimeUnit.Hour: binSizeSecs = binSize * secPerHour; break;
            case GroupByTimeUnit.Minute: binSizeSecs = binSize * secPerMinute; break;
            default:
                throw new QueryParseError(`Invalid time unit for groupBy: ${unit}. Expected one of hour, minute.`);
        }

        return `${column} / ${binSizeSecs} * ${binSizeSecs}`;
    }

    protected getDurationGroupBySql(field: keyof Round, binSize: number, unit: GroupByTimeUnit): string {
        const column = this.roundContext.getColumnName(field);
        let binSizeSecs: number;

        switch (unit) {
            case GroupByTimeUnit.Hour: binSizeSecs = binSize * secPerHour; break;
            case GroupByTimeUnit.Minute: binSizeSecs = binSize * secPerMinute; break;
            case GroupByTimeUnit.Second: binSizeSecs = binSize; break;
            default:
                throw new QueryParseError(`Invalid time unit for groupBy: ${unit}. Expected one of hour, minute, second.`);
        }

        return column + (binSizeSecs > 1 ? ` / ${binSizeSecs} * ${binSizeSecs}` : '');
    }

    protected getGroupBySpec(): GroupBySpec<Round> {
        return RoundGroupBySpec;
    }

    private formatGroupKey(value: unknown): any {
        const groupByType = RoundGroupBySpec[this.groupByCol.field];
        if (groupByType === GroupByFieldType.AbsoluteTime) {
            const unit = this.groupByCol.args[1] as GroupByTimeUnit;
            const binValue = value as number;
            switch (unit) {
                case GroupByTimeUnit.Year: return YearToUnixTimestamp(binValue);
                case GroupByTimeUnit.Month: return MonthToUnixTimestamp(binValue);
                case GroupByTimeUnit.Week: return WeekToUnixTimestamp(binValue);
                case GroupByTimeUnit.Day: return binValue * secPerDay;
                case GroupByTimeUnit.Hour: return binValue * secPerHour;
            }
        }

        // Other binned types are already mapped to the first second of their bin as part of the query
        return value;
    }
}
