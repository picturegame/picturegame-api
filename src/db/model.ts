import * as Api from '../models/model';
import { formatTimeDiff } from '../utils/TimeUtils';

export const enum TableName {
    Meta = 'meta',
    Rounds = 'rounds',
    Status = 'status',
    Users = 'users',
    RoundsView = 'v_rounds',
}

export const enum RoundFields {
    RoundNumber = 'round_number',
    PostUrl = 'post_url',
    ThumbnailUrl = 'thumbnail_url',
    Id = 'id',
    WinningCommentId = 'winning_comment_id',
    HostName = 'host_name',
    PostTime = 'post_time',
    WinnerName = 'winner_name',
    WinTime = 'win_time',
    PlusCorrectTime = 'plus_correct_time',
    Title = 'title',
    AbandonedTime = 'abandoned_time',
}

export const enum RoundViewFields {
    PostDelay = 'post_delay',
    SolveTime = 'solve_time',
    PlusCorrectDelay = 'plus_correct_delay',

    PostTimeOfDay = 'post_time_of_day',
    PostMonthOfYear = 'post_month_of_year',
    PostWeekOfYear = 'post_week_of_year',
    PostDayOfWeek = 'post_day_of_week',
    PostYear = 'post_year',
    PostMonth = 'post_month',
    PostWeek = 'post_week',
    PostDay = 'post_day',
    PostHour = 'post_hour',

    WinTimeOfDay = 'win_time_of_day',
    WinMonthOfYear = 'win_month_of_year',
    WinWeekOfYear = 'win_week_of_year',
    WinDayOfWeek = 'win_day_of_week',
    WinYear = 'win_year',
    WinMonth = 'win_month',
    WinWeek = 'win_week',
    WinDay = 'win_day',
    WinHour = 'win_hour',

    Abandoned = 'abandoned',
}

export interface Round extends Partial<RoundCalculatedColumns> {
    [RoundFields.RoundNumber]: number;

    [RoundFields.Title]: string | null;
    [RoundFields.PostUrl]: string | null;
    [RoundFields.ThumbnailUrl]: string | null;
    [RoundFields.Id]: string | null;
    [RoundFields.WinningCommentId]: string | null;

    [RoundFields.HostName]: string | null;
    [RoundFields.PostTime]: number | null;

    [RoundFields.WinnerName]: string | null;
    [RoundFields.WinTime]: number | null;
    [RoundFields.PlusCorrectTime]: number | null;
    [RoundFields.AbandonedTime]: number | null;
}

export interface RoundCalculatedColumns {
    [RoundViewFields.PostDelay]: number | null;
    [RoundViewFields.SolveTime]: number | null;
    [RoundViewFields.PlusCorrectDelay]: number | null;

    [RoundViewFields.PostTimeOfDay]: number | null;
    [RoundViewFields.PostMonthOfYear]: number | null;
    [RoundViewFields.PostWeekOfYear]: number | null;
    [RoundViewFields.PostDayOfWeek]: number | null;
    [RoundViewFields.PostYear]: number | null;
    [RoundViewFields.PostMonth]: number | null;
    [RoundViewFields.PostWeek]: number | null;
    [RoundViewFields.PostDay]: number | null;
    [RoundViewFields.PostHour]: number | null;

    [RoundViewFields.WinTimeOfDay]: number | null;
    [RoundViewFields.WinMonthOfYear]: number | null;
    [RoundViewFields.WinWeekOfYear]: number | null;
    [RoundViewFields.WinDayOfWeek]: number | null;
    [RoundViewFields.WinYear]: number | null;
    [RoundViewFields.WinMonth]: number | null;
    [RoundViewFields.WinWeek]: number | null;
    [RoundViewFields.WinDay]: number | null;
    [RoundViewFields.WinHour]: number | null;

    [RoundViewFields.Abandoned]: 0 | 1 | null;
}

export function toDbRound(apiRound: Api.Round): Round {
    return {
        [RoundFields.RoundNumber]: apiRound.roundNumber,
        [RoundFields.Title]: apiRound.title ?? null,
        [RoundFields.PostUrl]: apiRound.postUrl ?? null,
        [RoundFields.ThumbnailUrl]: apiRound.thumbnailUrl ?? null,
        [RoundFields.Id]: apiRound.id ?? null,
        [RoundFields.WinningCommentId]: apiRound.winningCommentId ?? null,
        [RoundFields.HostName]: apiRound.hostName ?? null,
        [RoundFields.PostTime]: apiRound.postTime ?? null,
        [RoundFields.WinnerName]: apiRound.winnerName ?? null,
        [RoundFields.WinTime]: apiRound.winTime ?? null,
        [RoundFields.PlusCorrectTime]: apiRound.plusCorrectTime ?? null,
        [RoundFields.AbandonedTime]: apiRound.abandonedTime ?? null,
    };
}

export const enum UserFields {
    Username = 'username',
    PasswordHash = 'password_hash',
}

export interface User {
    [UserFields.Username]: string;
    [UserFields.PasswordHash]: string;
}

export const enum MetaFields {
    TableName = 'table_name',
    Version = 'version',
}

export interface Meta {
    [MetaFields.TableName]: string;
    [MetaFields.Version]: number;
}

export const enum StatusFields {
    ServiceName = 'service_name',
    Version = 'version',
    StartTime = 'start_time',
}

export interface Status {
    [StatusFields.ServiceName]: string;
    [StatusFields.Version]: string;
    [StatusFields.StartTime]: number;
}

export function fromDbStatus(dbStatus: Status): Api.ServiceStatusResponse {
    return {
        name: dbStatus[StatusFields.ServiceName],
        version: dbStatus[StatusFields.Version],
        startTime: dbStatus[StatusFields.StartTime],
        uptime: formatTimeDiff(Math.floor(Date.now() / 1000) - dbStatus[StatusFields.StartTime]),
    };
}

export function toDbStatus(status: Api.ServiceStatusRequest): Status {
    return {
        [StatusFields.ServiceName]: status.name,
        [StatusFields.StartTime]: status.startTime,
        [StatusFields.Version]: status.version,
    };
}
