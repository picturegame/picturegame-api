import * as Knex from 'knex';
import { ColumnLookup, FieldSpec, TokenType } from 'picturegame-api-wrapper';

import { Round } from '../models/model';
import { QueryContext } from '../querylang/QueryBuilder';

import { Round as DbRound, RoundFields, RoundViewFields, TableName } from './model';

export const RoundFieldSpec: FieldSpec<Round> = {
    roundNumber: TokenType.NumericValue,

    title: TokenType.StringValue,
    postUrl: TokenType.StringValue,
    thumbnailUrl: TokenType.StringValue,
    id: TokenType.StringValue,
    winningCommentId: TokenType.StringValue,

    hostName: TokenType.StringValue,
    postTime: TokenType.DateTimeValue,

    winnerName: TokenType.StringValue,
    winTime: TokenType.DateTimeValue,
    plusCorrectTime: TokenType.DateTimeValue,
    abandonedTime: TokenType.DateTimeValue,

    solveTime: TokenType.NumericValue,
    plusCorrectDelay: TokenType.NumericValue,
    postDelay: TokenType.NumericValue,

    postDayOfWeek: TokenType.NumericValue,
    postTimeOfDay: TokenType.TimeValue,
    winDayOfWeek: TokenType.NumericValue,
    winTimeOfDay: TokenType.TimeValue,

    abandoned: TokenType.BooleanValue,
};

const RoundColumnLookup: ColumnLookup<Round> = {
    roundNumber: RoundFields.RoundNumber,

    title: RoundFields.Title,
    postUrl: RoundFields.PostUrl,
    thumbnailUrl: RoundFields.ThumbnailUrl,
    id: RoundFields.Id,
    winningCommentId: RoundFields.WinningCommentId,

    hostName: RoundFields.HostName,
    postTime: RoundFields.PostTime,

    winnerName: RoundFields.WinnerName,
    winTime: RoundFields.WinTime,
    plusCorrectTime: RoundFields.PlusCorrectTime,
    abandonedTime: RoundFields.AbandonedTime,

    solveTime: RoundViewFields.SolveTime,
    plusCorrectDelay: RoundViewFields.PlusCorrectDelay,
    postDelay: RoundViewFields.PostDelay,

    postDayOfWeek: RoundViewFields.PostDayOfWeek,
    postTimeOfDay: RoundViewFields.PostTimeOfDay,
    winDayOfWeek: RoundViewFields.WinDayOfWeek,
    winTimeOfDay: RoundViewFields.WinTimeOfDay,

    abandoned: RoundViewFields.Abandoned,
};

export class RoundQueryContext extends QueryContext<Round> {
    private columns = new Set<string>();
    private selectingAll = false;

    constructor(db: Knex) {
        super(db, db.from(TableName.RoundsView));
    }

    getColumnName(field: keyof Round): string {
        return RoundColumnLookup[field];
    }

    selectColumn(field: keyof Round) {
        this.columns.add(RoundColumnLookup[field]);
    }

    selectAll() {
        this.selectingAll = true;
    }

    protected convert(dbRound: DbRound) {
        return fromDbRound(dbRound);
    }

    protected prepare() {
        if (!this.selectingAll) {
            this.selectColumn('roundNumber');
            this.query.columns([...this.columns]);
        }
    }
}

function fromDbRound(dbRound: DbRound): Round {
    return {
        roundNumber: dbRound.round_number,
        title: nullToUndefined(dbRound.title),
        postUrl: nullToUndefined(dbRound.post_url),
        thumbnailUrl: nullToUndefined(dbRound.thumbnail_url),
        id: nullToUndefined(dbRound.id),
        winningCommentId: nullToUndefined(dbRound.winning_comment_id),
        hostName: nullToUndefined(dbRound.host_name),
        postTime: nullToUndefined(dbRound.post_time),
        winnerName: nullToUndefined(dbRound.winner_name),
        winTime: nullToUndefined(dbRound.win_time),
        abandonedTime: nullToUndefined(dbRound.abandoned_time),
        plusCorrectTime: nullToUndefined(dbRound.plus_correct_time),
        postDelay: nullToUndefined(dbRound.post_delay),
        solveTime: nullToUndefined(dbRound.solve_time),
        plusCorrectDelay: nullToUndefined(dbRound.plus_correct_delay),
        postDayOfWeek: nullToUndefined(dbRound.post_day_of_week),
        postTimeOfDay: nullToUndefined(dbRound.post_time_of_day),
        winDayOfWeek: nullToUndefined(dbRound.win_day_of_week),
        winTimeOfDay: nullToUndefined(dbRound.win_time_of_day),
        abandoned: typeof dbRound.abandoned === 'number' ? !!dbRound.abandoned : undefined,
    };
}

function nullToUndefined<T>(val: T | null): T | undefined {
    return val === null ? undefined : val;
}
