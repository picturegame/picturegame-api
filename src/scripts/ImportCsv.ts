import * as fs from 'fs';
import * as _ from 'lodash';
import * as Papa from 'papaparse';

import { Application } from '../application';
import * as model from '../models/model';

export async function importCsv(app: Application, fileName: string, cleanup: boolean = true) {
    const rounds = await loadRounds(fileName);
    await app.db.rounds.batchCreateRounds(rounds);
    if (cleanup) {
        await app.teardown();
    }
}

export async function updateFromCsv(app: Application, fileName: string) {
    const rounds = await loadRounds(fileName);
    await app.db.rounds.batchUpdateRounds(rounds);
    await app.teardown();
}

function parseCsvRound(row: any): model.Round {
    const round: model.Round = {
        roundNumber: parseInt(row.roundNumber, 10),
        title: row.title,
        id: row.id,
        postUrl: row.postUrl,
        hostName: row.hostName,
        postTime: row.postTime && parseInt(row.postTime, 10),
        winnerName: row.winnerName,
        winTime: row.winTime && parseInt(row.winTime, 10),
        winningCommentId: row.winningCommentId,
        plusCorrectTime: row.plusCorrectTime && parseInt(row.plusCorrectTime, 10),
    };
    if (isNaN(round.roundNumber) ||
        (round.postTime && isNaN(round.postTime)) ||
        (round.winTime && isNaN(round.winTime)) ||
        (round.plusCorrectTime && isNaN(round.plusCorrectTime))) {

        console.error(round);
        throw new Error('Invalid round');
    }

    return _.pickBy(round, f => !!f) as model.Round;
}

function loadRounds(fileName: string): Promise<model.Round[]> {
    return new Promise((resolve, reject) => {
        if (!fs.existsSync(fileName)) {
            return resolve([]);
        }

        fs.readFile(fileName, 'utf8', (error, data) => {
            if (error) {
                return reject(error);
            }

            const result = Papa.parse(data.trim(), { header: true, skipEmptyLines: true });
            const rounds: model.Round[] = [];

            result.data.forEach(raw => rounds.push(parseCsvRound(raw)));

            resolve(rounds);
        });
    });
}
