import * as _ from 'lodash';
import * as moment from 'moment';

import { BadRequestError } from '../models/Error';

import { validPositiveInteger, validUsername } from '../validation';

export function positiveInt(query: any, fieldName: string, required: boolean = false): number | undefined {
    const rawValue = extractStringOrThrow(query, fieldName, required);
    if (_.isNil(rawValue)) {
        return undefined;
    }
    return parseIntOrThrow(rawValue, fieldName, 1);
}

export function nonNegativeInt(query: any, fieldName: string, required: boolean = false): number | undefined {
    const rawValue = extractStringOrThrow(query, fieldName, required);
    if (_.isNil(rawValue)) {
        return undefined;
    }
    return parseIntOrThrow(rawValue, fieldName, 0);
}

export function username(query: any, fieldName: string, required: boolean = false): string | undefined {
    const value = extractStringOrThrow(query, fieldName, required);
    if (!value) {
        return undefined;
    }
    if (!value.match(validUsername)) {
        throw new BadRequestError(BadRequestError.InvalidUsername, { fieldName });
    }
    return value;
}

export function array(query: any, fieldName: string, required: boolean = false): string[] | undefined {
    const rawValue = query[fieldName];
    if (!rawValue) {
        if (required) {
            throw new BadRequestError(BadRequestError.ValueRequired, { fieldName });
        }
        return undefined;
    }
    if (_.isString(rawValue)) {
        return rawValue.split(',');
    }
    if (_.isArray(rawValue)) {
        return rawValue;
    }
    throw new BadRequestError(BadRequestError.ValueMustNotBeObject, { fieldName });
}

export function dateTime(query: any, fieldName: string, required: boolean = false): moment.Moment | undefined {
    const rawValue = extractStringOrThrow(query, fieldName, required);
    if (!rawValue) {
        return undefined;
    }
    const value = moment.utc(rawValue, moment.ISO_8601, true);
    if (!moment.isMoment(value) || !value.isValid()) {
        throw new BadRequestError(BadRequestError.InvalidDateTime, { fieldName });
    }
    return value;
}

export function bool(query: any, fieldName: string, defaultVal: boolean = false): boolean {
    const rawValue = query[fieldName] as string;
    if (!rawValue) {
        return defaultVal;
    }
    return rawValue === 'true';
}

export function extractStringOrThrow(query: any, fieldName: string, required: boolean = false): string | undefined {
    const rawValue = query[fieldName];
    if (!rawValue) {
        if (required) {
            throw new BadRequestError(BadRequestError.ValueRequired, { fieldName });
        }
        return undefined;
    }
    if (!_.isString(rawValue)) {
        throw new BadRequestError(BadRequestError.ValueMustNotBeObject, { fieldName });
    }
    return rawValue;
}

function parseIntOrThrow(rawValue: string, fieldName: string, minValue: number) {
    const value = parseInt(rawValue, 10);
    if (!rawValue.match(validPositiveInteger) || value < minValue) {
        throw new BadRequestError(BadRequestError.InvalidPositiveInt, { fieldName });
    }
    return value;
}
