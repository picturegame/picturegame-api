//////////////////////////////
// Data Types

export interface Round extends ComputedRoundFields {
    roundNumber: number;

    title?: string;
    postUrl?: string;
    thumbnailUrl?: string;
    id?: string;
    winningCommentId?: string;

    hostName?: string;
    postTime?: number;

    winnerName?: string;
    winTime?: number;
    plusCorrectTime?: number;
    abandonedTime?: number;
}

export interface ComputedRoundFields {
    postDelay?: number; // postTime - plusCorrectTime of prev round
    solveTime?: number; // winTime - postTime
    plusCorrectDelay?: number; // plusCorrectTime - winTime

    postDayOfWeek?: number; // 0 = sunday, 6 = saturday
    postTimeOfDay?: number; // seconds since midnight
    winDayOfWeek?: number;
    winTimeOfDay?: number;

    /**
     * True if winTime is after postTime of the following round,
     * or if winTime is null and the following round has been posted.
     * not based on plusCorrectTime, as we don't want round corrections to be picked up as abandonment
     */
    abandoned?: boolean;
}

export interface LeaderboardEntry {
    username: string;
    rank?: number;
    numWins: number;
    roundList?: number[];
    winTimeList?: number[];
}

export interface RoundAggregates {
    numRounds: number;

    minRoundNumber?: number;
    maxRoundNumber?: number;
    allRoundNumbers?: number;

    minPostTime?: number;
    maxPostTime?: number;

    minWinTime?: number;
    maxWinTime?: number;
    allWinTimes?: number[];

    avgSolveTime?: number;
    minSolveTime?: number;
    maxSolveTime?: number;

    avgPostDelay?: number;
    minPostDelay?: number;
    maxPostDelay?: number;

    avgPlusCorrectDelay?: number;
    minPlusCorrectDelay?: number;
    maxPlusCorrectDelay?: number;

    numHosts?: number;
    numWinners?: number;
}

export type RoundGroup = RoundAggregates & Partial<Round>;

export interface Player {
    username: string;

    stats: PlayerMetrics;
}

export interface PlayerMetrics {
    numRoundsWon: number;
    numRoundsHosted: number;

    avgSolveTime: number;
    minSolveTime: number;
    maxSolveTime: number;

    avgPostDelay?: number;
    minPostDelay?: number;
    maxPostDelay?: number;

    avgRoundLength?: number;
    minRoundLength?: number;
    maxRoundLength?: number;

    avgPlusCorrectDelay?: number;
    minPlusCorrectDelay?: number;
    maxPlusCorrectDelay?: number;
}

//////////////////////////////
// Request Types

export interface BulkRequest {
    requests: SingleRequest[];
}

export type RequestType = 'leaderboard' | 'rounds' | 'rounds/aggregate';
export interface SingleRequest {
    endpoint: RequestType;
}

export interface SingleLeaderboardRequest extends SingleRequest {
    endpoint: 'leaderboard';

    fromRank?: number;
    count?: number;
    players?: string[];
    ranks?: number[];

    includeRoundNumbers?: boolean;
    includeWinTimes?: boolean;

    filterRounds?: string;
}

export interface SingleRoundsRequest extends SingleRequest {
    endpoint: 'rounds';

    filter?: string;
    select?: string[];
    sort?: string[];

    limit?: number;
    offset?: number;
}

export interface SingleRoundsAggregateRequest extends SingleRequest {
    endpoint: 'rounds/aggregate';

    filter?: string;
    select?: string[];
    sort?: string[];
    groupBy?: string;
    groupFilter?: string;

    limit?: number;
    offset?: number;
}

//////////////////////////////
// Response Types

export interface LeaderboardResponse {
    leaderboard: LeaderboardEntry[];
    invalidRanks?: string[];
    invalidUsernames?: string[];
}

export interface LeaderboardCountResponse {
    numWinners: number;
}

export interface ListResponse<T> {
    totalNumResults: number;
    startIndex: number;
    pageSize: number;
    results: T[];
}

export interface UpdateRoundResponse {
    round: Round;
    newWinner?: LeaderboardEntry;
    oldWinner?: LeaderboardEntry;
}

export interface CurrentResponse {
    round: Round;
}

export interface MigrateResponse {
    newPlayer: LeaderboardEntry;
}

export interface ErrorResponse {
    errorCode: string;
    statusCode: number;
    message: string;
    data?: object;
}

export interface BulkResponse {
    responses: (LeaderboardResponse | ListResponse<Round> | ListResponse<RoundGroup> | ErrorResponse)[];
}

//////////////////////////////
// Meta Types

export interface ServiceStatusRequest {
    name: string;
    version: string;
    startTime: number;
}

export interface ServiceStatusResponse extends ServiceStatusRequest {
    uptime: string;
}

export interface StatusResponse {
    services: ServiceStatusResponse[];
}

export interface User {
    username: string;
    passwordHash: string;
}
