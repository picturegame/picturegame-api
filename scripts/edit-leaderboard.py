import json
import os
import praw
import requests

dirName = os.path.dirname(os.path.abspath(__file__))
fileName = os.path.join(dirName, '../config.json')

config = {}
with open(fileName) as f:
    config = json.loads(f.read())

subredditName = config['reddit']['subreddit']
redditAuth = config['reddit']['auth']
port = config['api']['port']

url = 'http://localhost:{}/leaderboard'.format(port)

request = requests.get(url)

if request.status_code != 200:
    raise Exception('Unexpected status code from PG API: {}'.format(request.status_code))

leaderboard = json.loads(request.text)['leaderboard']

reddit = praw.Reddit(
    user_agent = redditAuth['userAgent'],
    client_id = redditAuth['clientId'],
    client_secret = redditAuth['clientSecret'],
    username = redditAuth['username'],
    password = redditAuth['password'])

subreddit = reddit.subreddit(subredditName)
wikiPage = subreddit.wiki['leaderboard']

outputString = '# Leaderboard\n\n' + \
    'Rank | Username | Rounds won | Total |\n' + \
    '|:--:|:--:|:--|:--:|:--:|\n'

for player in leaderboard:
    outputString += '{} | {} | {} | {}\n'.format(
        player['rank'],
        player['username'],
        ', '.join(str(num) for num in player['roundList']),
        len(player['roundList']))

wikiPage.edit(outputString)

print('Done')
