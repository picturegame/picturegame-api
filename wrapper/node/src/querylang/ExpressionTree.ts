import * as Lang from './querylang';

export abstract class ExpressionTree {
    abstract isComplete(): boolean;
    abstract raw(): object;
}

export class BaseExpression extends ExpressionTree {
    constructor(public lValue: Lang.IToken, public operator: Lang.IToken, public rValue: RValueType) {
        super();
    }

    isComplete() {
        // ValueExpressions are the minimal unit, always constructed in their entirety
        return true;
    }

    raw() {
        return {
            lValue: this.lValue.input,
            operator: this.operator.input,
            rValue: this.rValue instanceof ArrayExpression
                ? this.rValue.items.map(i => i.input)
                : this.rValue.input,
        };
    }
}

export class CompoundExpression extends ExpressionTree {
    public rValue: ExpressionTree;
    constructor(public lValue: ExpressionTree, public operator: Lang.IToken) {
        super();
    }

    setRValue(rValue: ExpressionTree) {
        this.rValue = rValue;
    }

    isComplete() {
        // Constructed with an lValue and an operator, completed with an rValue
        return !!this.rValue;
    }

    raw() {
        return {
            lValue: this.lValue.raw(),
            operator: this.operator.input,
            rValue: this.rValue?.raw(),
        };
    }
}

export class ArrayExpression {
    readonly type = Lang.TokenType.Array;
    arrayType: Lang.TokenType;
    items: Lang.IToken[] = [];

    constructor(public index: number) {
    }

    tryAdd(token: Lang.IToken) {
        if (!token || !isValidRValue(token)) {
            throwForUnexpectedToken(token);
        }

        if (this.items.length === 0) {
            this.arrayType = token.type;
            this.items.push(token);
            return;
        }
        if (token.type !== this.arrayType) {
            throw new Lang.QueryParseError(
                `Mismatched type ${token.type} for array type ${this.arrayType} at index ${token.index}`);
        }
        this.items.push(token);
    }
}

export function isIdentifier(expr: RValueType): expr is Lang.IToken {
    return !(expr instanceof ArrayExpression) && expr.type === Lang.TokenType.Identifier;
}

export type RValueType = Lang.IToken | ArrayExpression;

export class ExpressionTreeBuilder {
    private bracketDepth: number = 0;
    private index: number = -1;
    constructor(private tokens: Lang.IToken[]) { }

    read(): ExpressionTree {
        const stack: ExpressionTree[] = [];

        while (this.index < this.tokens.length - 1) {
            const stackSize = stack.length;
            const tip = stack[stackSize - 1];
            const token = this.tokens[++this.index];

            if (token.type === Lang.TokenType.CloseBracket) {
                if (this.bracketDepth === 0 || stackSize !== 1 || !tip.isComplete()) {
                    throwForUnexpectedToken(token);
                }
                this.bracketDepth--;
                return tip;
            }

            if (token.type === Lang.TokenType.LogicalOperator) {
                if (stackSize === 0 || !tip.isComplete()) {
                    throwForUnexpectedToken(token);
                }
                stack.push(new CompoundExpression(stack.pop()!, token));
                continue;
            }

            if (token.type === Lang.TokenType.OpenBracket) {
                if (stackSize > 0 && tip.isComplete()) {
                    throwForUnexpectedToken(token);
                }
                this.bracketDepth++;
                stack.push(this.read());
            } else {
                stack.push(this.tryReadBaseExpr());
            }

            const nextToken = this.tokens[this.index + 1];
            if (nextToken
                && nextToken.type === Lang.TokenType.LogicalOperator
                && nextToken.input === Lang.LogicalOperatorType.And) {
                // Due to precedence rules, we need to place the current subtree as the lValue of the next AND tree.
                // In the case of an OR, we'd want to place the entire tree under the new OR tree
                continue;
            }
            squashStack(stack);
        }

        // EOI: Need a single complete tree and no unmatched brackets
        if (stack.length !== 1 || !stack[0].isComplete() || this.bracketDepth > 0) {
            throw new Lang.QueryParseError('Unexpected end of input');
        }

        return stack[0];
    }

    private tryReadBaseExpr(): BaseExpression {
        const lValue = this.tokens[this.index];
        if (lValue?.type !== Lang.TokenType.Identifier) {
            throwForUnexpectedToken(lValue);
        }

        const operator = this.tokens[++this.index];
        if (operator?.type !== Lang.TokenType.CompOperator) {
            throwForUnexpectedToken(operator);
        }

        const rValue = this.tokens[++this.index];
        if (rValue?.type === Lang.TokenType.OpenSqBrace) {
            const array = this.tryReadArray(rValue);
            return new BaseExpression(lValue, operator, array);
        }

        if (!rValue || !isValidRValue(rValue)) {
            throwForUnexpectedToken(rValue);
        }

        return new BaseExpression(lValue, operator, rValue);
    }

    private tryReadArray(openBrace: Lang.IToken): ArrayExpression {
        const array = new ArrayExpression(openBrace.index);

        let nextToken = this.tokens[++this.index];
        while (nextToken?.type !== Lang.TokenType.CloseSqBrace) {
            array.tryAdd(nextToken);
            nextToken = this.tokens[++this.index];
            if (!nextToken) {
                throwForUnexpectedToken(nextToken);
            }
            if (nextToken.type === Lang.TokenType.Comma) {
                nextToken = this.tokens[++this.index];
                continue;
            }
        }
        return array;
    }
}

function throwForUnexpectedToken(token: Lang.IToken) {
    if (!token) {
        throw new Lang.QueryParseError('Unexpected end of input');
    }
    throw new Lang.QueryParseError(`Unexpected token '${token.input}' of type ${token.type} at index ${token.index}`);
}

function isValidRValue(token: Lang.IToken) {
    return token.type === Lang.TokenType.DateTimeValue
        || token.type === Lang.TokenType.Identifier
        || token.type === Lang.TokenType.NumericValue
        || token.type === Lang.TokenType.StringValue
        || token.type === Lang.TokenType.BooleanValue
        || token.type === Lang.TokenType.TimeValue;
}

function squashStack(stack: ExpressionTree[]) {
    if (stack.length === 0) { return; } // Should never happen

    while (stack.length > 1
        && stack[stack.length - 1].isComplete()
        && stack[stack.length - 2] instanceof CompoundExpression) {

        (stack[stack.length - 2] as CompoundExpression).setRValue(stack.pop()!);
    }
}
