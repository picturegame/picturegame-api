import * as Lang from './querylang';

// tslint:disable-next-line
const DateTimeRegex = /^[0-9]{4}-[0-9]{2}-[0-9]{2}(?:T[0-9]{2}(?::[0-9]{2}(?::[0-9]{2}(?:\.[0-9]+)?)?)?(?:(?:[\+-][0-9]{2}(?::?[0-9]{2})?)|Z)?)?/;
const TimeRegex = /^[0-9]{2}:[0-9]{2}:[0-9]{2}/;

export class Tokenizer {
    private tokens: Lang.IToken[] = [];
    private index: number = 0;
    constructor(private input: string) {}

    tokenize() {
        while (this.index < this.input.length) {
            this.readNext();
        }
        return this.tokens;
    }

    private readNext() {
        const next = this.input[this.index++];

        if (isWhitespace(next)) {
            return;
        }
        if (isOpenBracket(next)) {
            this.tokens.push({
                index: this.index - 1,
                input: next,
                type: Lang.TokenType.OpenBracket,
            });
            return;
        }
        if (isCloseBracket(next)) {
            this.tokens.push({
                index: this.index - 1,
                input: next,
                type: Lang.TokenType.CloseBracket,
            });
            return;
        }
        if (isOpenSqBrace(next)) {
            this.tokens.push({
                index: this.index - 1,
                input: next,
                type: Lang.TokenType.OpenSqBrace,
            });
            return;
        }
        if (isCloseSqBrace(next)) {
            this.tokens.push({
                index: this.index - 1,
                input: next,
                type: Lang.TokenType.CloseSqBrace,
            });
            return;
        }
        if (isComma(next)) {
            this.tokens.push({
                index: this.index - 1,
                input: next,
                type: Lang.TokenType.Comma,
            });
            return;
        }
        if (isQuote(next)) {
            this.readToNextQuote(next);
            return;
        }
        if (isNumber(next)) {
            this.readNumberOrDateTime();
            return;
        }
        if (isWord(next)) {
            this.readWord();
            return;
        }
        throw new Lang.QueryParseError(`Unrecognized char ${next} at index ${this.index - 1}`);
    }

    private readToNextQuote(quote: string) {
        const nextQuoteIndex = this.input.indexOf(quote, this.index);
        if (nextQuoteIndex === -1) {
            throw new Lang.QueryParseError(`No matching quote for opening quote at index ${this.index - 1}`);
        }
        const input = this.input.substring(this.index, nextQuoteIndex);
        this.tokens.push({
            index: this.index - 1,
            input,
            type: Lang.TokenType.StringValue,
        });
        this.index = nextQuoteIndex + 1;
    }

    private readNumberOrDateTime() {
        this.index--; // Ensure we re-read the first char

        const trimmedInput = this.input.substring(this.index);

        const dateMatch = trimmedInput.match(DateTimeRegex);
        if (dateMatch) {
            this.tokens.push({
                index: this.index,
                input: dateMatch[0],
                type: Lang.TokenType.DateTimeValue,
            });
            this.index += dateMatch[0].length;
            return;
        }

        const timeMatch = trimmedInput.match(TimeRegex);
        if (timeMatch) {
            this.tokens.push({
                index: this.index,
                input: timeMatch[0],
                type: Lang.TokenType.TimeValue,
            });
            this.index += timeMatch[0].length;
            return;
        }

        const numberMatch = trimmedInput.match(/^[0-9]+/)!;
        this.tokens.push({
            index: this.index,
            input: numberMatch[0],
            type: Lang.TokenType.NumericValue,
        });
        this.index += numberMatch[0].length;
    }

    private readWord() {
        this.index--; // Ensure we re-read the first char

        const match = this.input.substring(this.index).match(/^\w+/)!;
        this.tokens.push({
            index: this.index,
            input: match[0],
            type: getTokenType(match[0]),
        });
        this.index += match[0].length;
    }
}

function getTokenType(input: string): Lang.TokenType {
    if (Lang.CompOperators.has(input)) {
        return Lang.TokenType.CompOperator;
    }
    if (Lang.LogicalOperators.has(input)) {
        return Lang.TokenType.LogicalOperator;
    }
    if (Lang.DirectionOperators.has(input)) {
        return Lang.TokenType.DirectionOperator;
    }
    if (Lang.BoolValues.has(input)) {
        return Lang.TokenType.BooleanValue;
    }
    return Lang.TokenType.Identifier;
}

function isWhitespace(char: string) {
    return !!char.match(/\s/);
}

function isOpenBracket(char: string) {
    return char === '(';
}

function isCloseBracket(char: string) {
    return char === ')';
}

function isOpenSqBrace(char: string) {
    return char === '[';
}

function isCloseSqBrace(char: string) {
    return char === ']';
}

function isComma(char: string) {
    return char === ',';
}

function isQuote(char: string) {
    return char === '"' || char === "'";
}

function isNumber(char: string) {
    return !!char.match(/\d/);
}

function isWord(char: string) {
    return !!char.match(/\w/);
}
