# picturegame-api

REST API for accessing statistical data for /r/PictureGame

## Dependencies
* node 8 LTS or 10 LTS
* yarn
* sqlite3

## Installation and building
* `yarn`
* `yarn build:prod`

## Setup and configuration
* Place a config file named `config.json` at the root directory (the same directory as this readme)
  * Config keys and values are defined in `src/index.d.ts`

## Running
* `yarn start`

## Running the docs with hot-reloading
* `yarn docs`

### To run tests
* `yarn test`

Alternatively, run either the API or the tests in debug mode using VSCode.

### To access docs
* Assuming you have the API running, point your browser at `localhost:9001/docs`

### Migrating data from v1.0
As of v1.1, the API uses an Sqlite3 database to store round information, instead of the csv data file used in 1.0.
To migrate data from the old csv format to a new Sqlite3 database run `yarn start importcsv filename.csv`, replacing `filename.csv` with the name of your data file.

### Creating an account to use for local testing
As of v1.1, authentication is required for all methods that make changes to the database. Currently these are POST, PATCH and DELETE /rounds, and POST /players/migrate.
To create an account, run `yarn start createuser username password`, replacing `username` and `password` with sensible values.
The API uses basic auth.

## Contributing
Please see [here](https://gitlab.com/picturegame/picturegame-api/issues?label_name%5B%5D=accepting+PRs) for a list of issues that I am currently accepting PRs for.

Before submitting a pull request, ensure the following things are done:

* If your changes add new public-facing functionality (e.g. a new endpoint, or a new parameter) they must be documented
* All changes must be tested (if you think you have some special exception to that rule, talk to me **before** submitting the PR)
* All existing and new tests must pass
* Linting must pass (run `yarn lint`)
